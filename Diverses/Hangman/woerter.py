text = """01 	Alpheratz
02 	Ankaa
03 	Schedar
04 	Diphda
05 	Achernar
06 	Hamal
07 	Acamar
08 	Menkar
09 	Mirfac
10 	Aldebaran
11 	Rigel
12 	Capella
13 	Bellatrix
14 	Elnath
15 	Alnilam
16 	Betelgeuse
17 	Canopus
18 	Sirius
19 	Adhara
20 	Procyon
21 	Pollux
22 	Avior
23 	Suhail
24 	Miaplacidus
25 	Alphard
26 	Regulus
27 	Dubhe
28 	Denebola
29 	Gienah
30 	Acrux
31 	Gacrux
32 	Alioth
33 	Spica
34 	Alkaid
35 	Hadar
36 	Menkent
37 	Arcturus
38 	Rigil-Kentaurus
39 	Zubenelgenubi
40 	Kochab
41 	Alpheca
42 	Antares
43 	Atria
44 	Sabic
45 	Shaula
46 	Rasalhague
47 	Eltanin
48 	Kaus
49 	Vega
50 	Nunki
51 	Altair
52 	Peacock
53 	Deneb
54 	Enif
55 	Al-Na'ir
56 	Fomalhaut
57 	Markab """

sterne = text.split()
sterne = sterne[1::2]

eissorten = ["Schokolade", "Erdbeer", "Cashew", "Blaubeer", "Banane", "Zitrone"]

dramatiker_source = """
    Aeschylus
    Euripides
    Sophocles
"""
dramatiker = dramatiker_source.split()
