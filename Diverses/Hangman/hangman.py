# Hangman
# 27.03.2023
# by piko

# wir brauchen ein Wort
from woerter import dramatiker
from random import choice, randint
import bilder
from turtle import *

FEHLSCHLAEGE_MAX = len(bilder.asciibilder)


wort = choice(dramatiker)
print(wort)
wort = wort.upper()


bereits_versucht = []
fehlschlaege = 0

anzeige = "_" * len(wort)

# große Schleife
while anzeige != wort:
    # buchstabe abfragen
    buchstabe = input("Bitte Buchstabe eingeben! ").upper()
    if len(buchstabe) > 1:
        print("Input war zu lang!")
    elif len(buchstabe) == 0:
        print("Kein Input.")
    else:
        # if buchstabe is alphabetical
        if buchstabe.isalpha():
            # testen, ob buchstabe schonmal vorkam
            if buchstabe in bereits_versucht:
                print("Diesen Buchstaben hatten wir schon!")
            else:
                bereits_versucht.append(buchstabe)
                # testen, ob der buchstabe im Wort ist
                if buchstabe in wort:
                    # wenn ja: in die anzeige einfügen
                    for stelle in range(len(wort)):
                        if wort[stelle] == buchstabe:
                            anzeige = anzeige[:stelle] + buchstabe + anzeige[stelle+1:]
                    print(anzeige)
                else:
                    # wenn nein: fehlschlag-counter erhöhen
                    print("Dieser Buchstabe kommt nicht im Wort vor.")
                    print(bilder.asciibilder[fehlschlaege])
                    fehlschlaege = fehlschlaege + 1
                    print(f"Fehlschläge: {fehlschlaege}")
                    # wenn fehlschläge zu hoch: abbrechen;
                    if fehlschlaege > FEHLSCHLAEGE_MAX:
                        print("Zu viele Fehlversuche!")
                        break
        else:
            print("Kein Buchstabe!")
               
# lösung zeigen
print(wort)
# zeigen, wie viele Fehlschläge


 
print(bereits_versucht)
for buchstabe in bereits_versucht:
    pu()
    goto(randint(-300, 300), randint(-300, 300))
    write(buchstabe, move=False, align="left", font=("Arial", 30, "normal"))
