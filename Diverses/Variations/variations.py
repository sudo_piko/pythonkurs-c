# Piko
# Python-Kurs, 29.03.2023
# Nachbau von: https://botsin.space/@variations_on_variations
# Video dazu: 

# Erstmal anschauen: es sind bunte, regelmäßige Vielecke, die übereinander liegen.
# Die Seiten sind immer gleich lang.
# Die Farben sind zufällig. => Funktion Zufallsfarbe
# Das Vieleck, das die meisten Ecken hat, liegt ganz unten;
# muss also zuerst gemalt werden.
# Jedes Vieleck hat eine Kante mit dem vorigen gemeinsam. Die Kante ist aber zufällig.




from polygone import fuell_n_eck
import random
from turtle import *
# oder sogar nur: from turtle import fd
# wir brauchen ja nur fd...


SEITENLAENGE = 100
MIN_ECKEN = 6
MAX_ECKEN = 8


def zufallsfarbe():
    farbtupel = (random.random(), random.random(), random.random())
    return farbtupel  


# wie viele Ecken?
tiefe = random.randint(MIN_ECKEN, MAX_ECKEN) # irgendeine Zahl zwischen 6 und 8
print(tiefe) 

# Wir fangen also mit dem äußersten Vieleck an und malen dann das nächstkleinere.
# Äußerstes Vieleck => größte Eckenzahl.
# Deshalb fängt die range-Funktion mit tiefe an,
# zählt runter bis Kurzvorzwei (also drei :)
# runterzählen: durch die -1. Wir gehen in "Minus-Einser-Schritten" vorwärts.
# Also rückwärts in Einserschritten.
for i in range(tiefe, 2, -1): 
#     print(i) # macht die for-Schleife, was ich will?
    fuell_n_eck(SEITENLAENGE, i, zufallsfarbe()) # in i ist also zb 8, 7, 6, 5, 4, 3 – jeweils nacheinander.
    zufaellige_kante = random.randint(0, i) # i ist ja die anzahl der ecken, demnach auch die anzahl der kanten.
    for j in range(zufaellige_kante):
        fd(SEITENLAENGE)
        lt(360/i)
        
    
# fertig! :)