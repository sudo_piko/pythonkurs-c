
from turtle import *
# import random

def fuelldreieck(l, farbe="black"):
    color(farbe)
    begin_fill()
    for i in range(3):
        fd(l)
        lt(120)
    end_fill()
        
def fuellviereck(l, farbe="black"):
    color(farbe)
    begin_fill()
    for i in range(4):
        fd(l)
        lt(90)
    end_fill()

def fuellfuenfeck(l, farbe="black"):
    color(farbe)
    begin_fill()
    for i in range(5):
        fd(l)
        lt(72)
    end_fill()
    
def fuellsechseck(l, farbe="black"):
    color(farbe)
    begin_fill()
    for i in range(6):
        fd(l)
        lt(60)
    end_fill()
    
# fuelldreieck(100)
# fuellviereck(100)
# fuellfuenfeck(100)
# fuellsechseck(100)


def fuell_n_eck(l, ecken=3, farbe="black"):
    # in der Aufgabenstellung gab es nur das Argument 'ecken'. Dann wäre die Länge immer fest
    # und die Farbe auch; eins könnte aber auch farbe hier zufällig bestimmen:
#     farbe = (random.random(), random.random(), random.random())
    color(farbe)
    begin_fill()
    for i in range(ecken):  # wird so oft ausgeführt, wie es ecken gibt.
        fd(l)
        lt(360/ecken)
        # Wie komme ich auf 360/ecken?
        # jede Ecke hat den gleichen Winkel und am Ende muss die Turtle sich einmal 
        # ganz um sich selbst gedreht haben (das sieht eins, wenn es den Einzelfunktionen
        # oben bei der Ausführung zuguckt. Je mehr Ecken, desto mehr sieht das auch aus,
        # als würde die Turtle einmal im Kreis laufen => 360° insgesamt, aufgeteilt in x
        # gleiche Teile. Und die Anzahl der Teile entspricht ja der Anzahl der ecken!
    end_fill()

# for i in range(2, 9):
#     fuell_n_eck(100, i)
