import turtle

def gestrichelte_linie(laenge, anzahl_striche):
    anzahl_leer = anzahl_striche - 1
    strichlaenge = laenge / (anzahl_striche + anzahl_leer)
    
    for i in range(anzahl_leer):
        turtle.pd()
        turtle.fd(strichlaenge)
        turtle.pu()
        turtle.fd(strichlaenge)
    turtle.pd()
    turtle.fd(strichlaenge)
    

if __name__ == "__main__":
    gestrichelte_linie(500, 13)