## 1 – Truchet Tiles
#
#
# In den Hausaufgaben vom 19. Januar gab es eine Aufgabe, in der ihr eine Funktion bauen solltet, die die Turtle einen schwarz-weiß halbiertes Quadrat malt.
# Wenn ihr diese Aufgabe nicht gelöst habt oder den euren Code nicht mehr habt, findet ihr hier eine (nicht so elegante)
# Lösung:
# https://gitlab.com/sudo_piko/pythonkurs-c/-/blob/main/Protokolle/Protokoll_2023-01-26.md
#
#
# - Verändert das Programm so, dass die einzelnen Kacheln keinen schwarzen Rand haben. Es soll also nur ein schwarzes
#   Dreieck sichtbar sein.
# - Verändert die Funktion mit den zufälligen Kacheln so, dass Zufall das default-Argument ist.
# - Macht eine fläche mit diesen Kacheln, sodass sie automatisch einstellen können, wie breit und wie hoch die fläche
#   ist - und die
#   Kacheln sollen zufällig sein. Für die Fläche könnt ihr euch an dem Programm orientieren, indem wir ein Punkte-Grid (15.12.2022, Aufgabe 3) gemacht haben.
# - Baut eine Version dieses Programms, in dem die Kacheln nicht zufällig sind, sondern nach einem bestimmten Vorschrift. Zum Beispiel immer abwechselnd, Oder abhängig davon, ob in einem bestimmten Text als Nächstes ein Vokal oder ein Konsonant kommt.
#
# - schaut euch folgendes Video an und überlegt, was wir an eurem Programm verändern müsstet, um die anderen Kachelformen zu erreichen.
# https://media.ccc.de/v/rc3-501024-operation_mindfuck_vol_4#t=680
#
# - Schreibt ein Programm, mit den schrägen Linien als Kacheln. (was im video aussieht wie ein Labyrinth.) Da habt ihr
#   jetzt nur zwei unterschiedliche Kacheln.
#
# <details>
#   <summary>Tipps </summary>
# Denkt daran, wie wir die anderen Kacheln entwickelt haben: wir haben zuerst nur ein Programm geschrieben, dass eine Kachel anzeigt. Dann haben wir dieses Programm in eine Funktion getan. Und dann haben wir eine weitere Funktion geschrieben, die die anderen Kacheln malt. Dann haben wir eine Funktion geschrieben, die eine zufällige Kachel auswählt. Erst dann Haben wir eine Fläche damit gefüllt…
#
# </details>
from math import sqrt
# Lösung
from turtle import *
from random import randint


speed(0)
def kachel(ecke=0, laenge=100):
    # Kachel
    p = pos()
    pu()
    for i in range(ecke):
        fd(laenge)
        lt(90)
    begin_fill()
    fd(laenge)
    lt(90)
    fd(laenge)
    lt(90)
    end_fill()

    pu()
    goto(p)
    setheading(0)

def zufallkachel(laenge=100):
    kachel(randint(0, 3), laenge)

def flaeche(hoehe, breite, kachelbreite=100):
    for i in range(hoehe):
        for j in range(breite):
            zufallkachel(kachelbreite)
            fd(kachelbreite)
        pu()
        bk(breite * kachelbreite)
        rt(90)
        fd(kachelbreite)
        lt(90)
        pd()

#flaeche(5, 5, 100)
def flaeche_rotierende_kacheln(hoehe, breite, kachelbreite=100):
    counter = 0
    for i in range(hoehe):
        for j in range(breite):
            kachel(ecke=counter % 4, laenge=kachelbreite)
            counter += 1
            fd(kachelbreite)
        pu()
        bk(breite * kachelbreite)
        rt(90)
        fd(kachelbreite)
        lt(90)
        pd()

# flaeche_rotierende_kacheln(5, 5, 100)

# flaeche mit Text
def flaeche_text(hoehe, breite, kachelbreite=100):
    text = "Hallo"
    counter = 0
    for i in range(hoehe):
        for j in range(breite):
            if text[counter % len(text)] in "aeiou":
                kachel(ecke=0, laenge=kachelbreite)
            else:
                kachel(ecke=1, laenge=kachelbreite)
            counter += 1
            fd(kachelbreite)
        pu()
        bk(breite * kachelbreite)
        rt(90)
        fd(kachelbreite)
        lt(90)
        pd()

# flaeche_text(5, 5, 100)

# flaeche mit schrägen Linien

def kachel_schraege_linien(laenge=100, ausrichtung=0):
    p = pos()
    pu()
    if ausrichtung > 1:
        ausrichtung = ausrichtung % 2
    if ausrichtung == 1:
        fd(laenge)
        lt(90)
    lt(45)
    pd()
    fd(laenge*sqrt(2))
    pu()
    lt(135)
    fd(laenge)
    if ausrichtung == 0:
        lt(90)
        fd(laenge)
    lt(90)


def flaeche_schraegen_linien(hoehe, breite, kachelbreite=100):
    counter = 0
    for i in range(hoehe):
        for j in range(breite):
            kachel_schraege_linien(laenge=kachelbreite, ausrichtung=randint(0,1)) # hier liegt der Hund begraben: counter % 2, random.randint(0, 1)
            counter += 1
            fd(kachelbreite)
        pu()
        bk(breite * kachelbreite)
        rt(90)
        fd(kachelbreite)
        lt(90)
        pd()


flaeche_schraegen_linien(5, 5, 100)

done()


