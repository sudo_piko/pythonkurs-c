# Aufgaben am 22.12.2022

Dateien aus dem Video:  
https://diode.zone/w/kRPgpbn2AZDuEV6tPLFjAU


```python
from turtle import *
import random

setup(500, 500, 2560, 0)

l = 50
hoehe = 5
breite = 3
speed(0)


def ttile1(seitenlaenge=200):
#     pd()
#     pensize(1)
#     for i in range(4):
#         fd(seitenlaenge)
#         lt(90)

    for i in range(2):
        pu()
        fd(seitenlaenge/2)
        lt(90)
        pd()
        pensize(seitenlaenge/10)
        circle(-seitenlaenge/2, 90)
        pu()
        lt(90)
        fd(seitenlaenge/2)
        lt(90)

def ttile2(seitenlaenge=200):
    # turtle muss irgendwo hin
    fd(seitenlaenge)
    lt(90)
    # tile1 malen
    ttile1(seitenlaenge)
    # turtle muss wieder zurück
    rt(90)
    fd(-seitenlaenge)
    

for i in range(hoehe):
    for j in range(breite):
        pu()
        goto(j*l, i*l)
        
        f = random.choice((ttile1, ttile2))
        f(l)
```
```python
from turtle import *
import random

setup(700, 700, 2560, 0)

l = 50
hoehe = 5
breite = 3
speed(0)


def ttile1(seitenlaenge=200):
#     pd()
#     pensize(1)
#     for i in range(4):
#         fd(seitenlaenge)
#         lt(90)

    for i in range(2):
        pu()
        fd(seitenlaenge/2)
        lt(90)
        pd()
        pensize(seitenlaenge/10)
        circle(-seitenlaenge/2, 90)
        pu()
        lt(90)
        fd(seitenlaenge/2)
        lt(90)

def ttile2(seitenlaenge=200):
    # turtle muss irgendwo hin
    fd(seitenlaenge)
    lt(90)
    # tile1 malen
    ttile1(seitenlaenge)
    # turtle muss wieder zurück
    rt(90)
    fd(-seitenlaenge)
    

counter = 0

for i in range(hoehe):
    for j in range(breite):
        pu()
        goto(j*l, i*l)
        print(counter%4)
        if counter%4 == 0:
            ttile1(l)
        else:
            ttile2(l)
        counter = counter + 1
```