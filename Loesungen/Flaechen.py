# Dieser Code ist als Erklärung für Hausaufgaben vom 2.2. entstanden, aber mit Flächen jonglieren wir schon ein bisschen
# länger herum. Der Code wird in folgenden Videos erklärt:
# https://diode.zone/w/hXfudXPVqyqq68RNPc7RQM
# https://diode.zone/w/c7ZCYQcjHpcg1wACjuw7fK


# Version 1: Aus Turtle-Sicht

from turtle import *

setup(width=500, height=500, startx=2560, starty=0)
pensize(3)
speed(8)

seitenlaenge = 30
reihenlaenge = 8
hoehe = 5


def quadratkreis(l):
    speed(0)
    for i in range(4):
        fd(l)
        lt(90)
    fd(l / 2)
    circle(l / 2)
    fd(-l / 2)

    speed(2)


for j in range(hoehe):
    #     print("Ich male jetzt Zeile", j)
    for i in range(reihenlaenge):
        quadratkreis(seitenlaenge)
        fd(seitenlaenge)
    # Bewegung zurück und in die nächste Reihe
    fd(-reihenlaenge * seitenlaenge)
    lt(90)
    fd(seitenlaenge)
    rt(90)

for i in range(hoehe):
    # hier kommt eine Reihe hin
    ...


# Version 2: Mit goto()

from turtle import *

setup(width=500, height=500, startx=2560, starty=0)
pensize(3)
speed(8)

seitenlaenge = 30
reihenlaenge = 8
hoehe = 5


def quadratkreis(l):
    speed(0)
    for i in range(4):
        fd(l)
        lt(90)
    fd(l / 2)
    circle(l / 2)
    fd(-l / 2)

    speed(2)


for j in range(hoehe):
    for i in range(reihenlaenge):
        goto(i * seitenlaenge, j * seitenlaenge)
        pd()
        quadratkreis(seitenlaenge)
        pu()



