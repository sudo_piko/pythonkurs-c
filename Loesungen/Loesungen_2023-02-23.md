# Aufgaben am 22.12.2022

Dateien aus den Videos:  
https://diode.zone/w/kRPgpbn2AZDuEV6tPLFjAU



Hausaufgabenbesprechung:  
- 1: Funktionen: https://diode.zone/w/u6cmPPQLFfUedK6TiQkYN2
- 2: Temperaturskalen übersetzen: https://diode.zone/w/shh1ZuQEvdvGKcV4ZUAD5b
- 3: Unregelmäßige Kreise: https://diode.zone/w/mvnn5kuyCjH2AeoBYw9EZQ


```python
def aufaddieren(zahl):
    ergebnis = 0
    for i in range(zahl+1):
        ergebnis = ergebnis + i
        print("i ist:", i, "ergebnis ist:", ergebnis)
    return ergebnis

def fakultaet(zahl):
    ergebnis = 1
    for i in range(1, zahl+1):
        ergebnis = ergebnis * i
        print("i ist:", i, "ergebnis ist:", ergebnis)
    return ergebnis

print(fakultaet(5))

# 
# counter = 0
# for i in range(20):
#     counter = counter + 1
#     print(counter)
```
```python
def celsius_zu_fahrenheit(gradcelsius):
    gradfahrenheit = gradcelsius * (9/5) + 32
    return gradfahrenheit

def fahrenheit_zu_celsius(gradfahrenheit):
    gradcelsius = (gradfahrenheit - 32) * (5/9)
    return gradcelsius

print(fahrenheit_zu_celsius(100))
```
```python
def skaliere(zahl, ursprungsbereich=(0, 100), zielbereich=(32, 212)):  # helper
    zahl = zahl - ursprungsbereich[0]
    zahl = zahl / (ursprungsbereich[1] - ursprungsbereich[0])
    zahl = zahl * (zielbereich[1] - zielbereich[0])
    zahl = zahl + zielbereich[0]    
    return zahl
    

def cels_zu_fah(temp):  #wrapper
    fah = skaliere(temp, ursprungsbereich=(0, 100), zielbereich=(32, 212))
    return fah

def fah_zu_cels(temp):
    cels = skaliere(temp, ursprungsbereich=(32, 212), zielbereich=(0, 100))
    return cels
    

print(cels_zu_fah(-273.150))
```
```python
from turtle import *
from random import random, randint
speed(0)

def unregelmaessiger_kreis1(radius, n_abschnitte=20):
    for i in range(n_abschnitte):
        color(random(), random(), random())
        pensize(randint(1, 50))
        circle(radius, 360/n_abschnitte)
    

def unregelmaessiger_kreis2(radius, n_kreise=20, max_kreisgroesse=100):
    for i in range(n_kreise):
        circle(randint(10, max_kreisgroesse))
        pu()
        circle(radius, 360/n_kreise)
        pd()
    
def unregelmaessiger_kreis3(radius, n_kreise=20, max_kreisgroesse=100):
    # nicht so hübsch
    for i in range(n_kreise):
        if max_kreisgroesse < 10:
            kleiner_radius = 10
        else:
            kleiner_radius = randint(10, max_kreisgroesse)
        if kleiner_radius > 50:
            unregelmaessiger_kreis3(kleiner_radius, 20, int(kleiner_radius/2))
        else: 
            circle(kleiner_radius)
        pu()
        circle(radius, 360/n_kreise)
        pd()

def unregelmaessiger_kreis4(radius, n_abschnitte=60):
    for i in range(n_abschnitte):
        wuerfel = randint(0, 1)
        if wuerfel == 0:
            pd()
        else:
            pu()
        circle(radius, 360/n_abschnitte)
        
def unregelmaessiger_kreis5(radius):
    abgelaufener_winkel = 0
    while abgelaufener_winkel < 360:
        wuerfel = randint(0, 1)
        if wuerfel == 0:
            pd()
        else:
            pu()
        winkel = randint(1, 30)
        if abgelaufener_winkel + winkel > 360:
            winkel = 360 - abgelaufener_winkel
        circle(radius, winkel)
        abgelaufener_winkel = abgelaufener_winkel + winkel
        
def unregelmaessiger_kreis6(radius, n_kreise=20, max_kreisgroesse=100):
    for i in range(n_kreise):
        unregelmaessiger_kreis5(randint(10, max_kreisgroesse))
        pu()
        circle(radius, 360/n_kreise)
        pd()

unregelmaessiger_kreis6(200)
```