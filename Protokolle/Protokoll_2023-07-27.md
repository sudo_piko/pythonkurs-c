# Pikos Python Kurs am 27.07.2023

# Wie weiter?
## Hinweise
- bleibt als Gruppe zusammen und sucht Euch zusammen neue Herausforderungen (siehe unten). Wenn Eure Gruppe zu klein wird, haut gerne Piko an, ob noch andere Gruppen Leute suchen.
- bleibt dran! Wenn Ihr Euch damit nicht beschäftigt, vergesst Ihr es allmählich. Es wieder neu zu lernen, wird zwar schneller gehen als beim ersten Mal, aber muss halt trotzdem auch getan werden. 
- Tutorials: Sind sicher interessant, aber sie bringen Euch selten Selbstständigkeit bei, sondern zeigen meistens nur, wie eins das machen würde. Versucht auch bei Tutorials (genau wie ich das bei meinen Videos ab und zu sage), anzuhalten und zu überlegen, wie Ihr das machen würdet. Kopiert möglichst keinen Code, sondern tippt ihn ab, so muss er zumindest einmal durch Euren Kopf...
- Wenn Ihr Lust habt, meldet Euch gerne (per Mail: piko@riseup.net) für den nächsten Kurs an. Wir fangen wieder bei Null an, aber das kann ja auch mal ganz interessant sein. Oder Ihr guckt Euch die Aufgaben von den alten Kursen an.

## Ressourcen zum Weiterlernen
### Python
- Rekursion
  - https://www.inf-schule.de/algorithmen/rekursivealgorithmen
  - https://inventwithpython.com/blog/2022/03/09/make-fractals-in-python-turtle-with-the-fractalartmaker-module/
- Kleine Übungsaufgaben
    - Pikos frühere Kurse: 
        - Abraxas: https://gitlab.com/sudo_piko/pykurs
        - Bython: https://gitlab.com/sudo_piko/pythonkurs-b
    - Project Euler (mathematiklastig): https://projecteuler.net/
    - Diverse Ideen: https://md.ha.si/9L_GnOccQCOMxE_bwkgkAQ#
- spezielle Themen: Es gibt wahnsinnig viele Ressourcen zu Python im Netz. Werft einfach die entsprechenden Wörter in eine Suchmaschine Eurer Wahl, eventuell noch mit "example" oder "tutorial" zB "Python pygame tetris tutorial"
    - Genart: https://www.youtube.com/watch?v=Cm_SzDlQ2cM
    - Nerdiges: Conway's Game of life
    - Spiele, zB Schere-Stein-Paper etc.
    - Rekursion: https://mundyreimer.github.io/blog/lindenmayer-grammars-1
- Bücher von Al Sweigart (auf Englisch): https://inventwithpython.com/
- weitere Kurse auf Webseiten:
    -  Deutschsprachig
        -  Ausführlich, viele Spiele: https://www.python-lernen.de
        -  Kurz, einfach, interaktiv: https://cscircles.cemc.uwaterloo.ca/de/
        -  Für Kinder gedacht => anschaulich erklärt: https://open.hpi.de/courses/pythonjunior2020
        -  Recht kondensiert, viel Inhalt, wenig Übungen: https://www.python-kurs.eu/
        -  Sehr gründlich, wirkt etwas trocken: https://openbook.rheinwerk-verlag.de/python/ 
    -  Englischsprachig
        -  https://www.learnpython.org/
        -  Tutorial mit Jupyter Notebooks (statt Thonny): https://www.youtube.com/watch?v=Z1Yd7upQsXY
        -  w3schools; interaktiv, ausführlich: https://www.w3schools.com/python/default.asp
        -  ganzer Kurs, mit Lessons, Quizzes und Aufgaben: https://www.codecademy.com/learn/learn-python-3


### Computer allgemein
- Lernspielesammlung: https://github.com/yrgo/awesome-educational-games








## Alle Pads

https://md.ha.si/dREUJnkwRb6cKhBtGG4Z5A#
https://md.ha.si/u2NJaOiDT06mebkFTxA_iw#
https://md.ha.si/AO2e_MfpSSW033tLJ8cGKg#
https://md.ha.si/TyTRRZjPSJqAdKp3GZSCIg#
https://md.ha.si/sQHFY5kiRV-WPRii3DwX-A#
https://md.ha.si/3LnMDXPYS1Gg1d6PHugleg#

## Pikos Dateien
... findet Ihr im Ordner `Sounds`