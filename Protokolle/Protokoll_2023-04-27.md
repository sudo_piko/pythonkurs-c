# Pikos Python Kurs am 27.04.2023

## Pads



## Pikos Datei
```python
# Definiert eine Funktion, die eine zufällige Zahl zwischen 23 und 42 ausgibt, die durch 3 teilbar ist:
#
# Baut eine Version, in der einfach so lange gewürfelt wird, bis eine Zahl rauskommt, die durch 3 teilbar ist.
# Baut eine zweite Version, die nur einmal würfelt und dann eine Zahl daraus macht, die zwischen 23 und 42 und durch 3 teilbar ist.


import random

def test2342(zahl):
    if 23 <= zahl <= 42:
        return True
    else:
        return False
#     if 23 <= zahl and zahl <= 42:
#
#     if 23 <= zahl:
#         if zahl <= 42

def test3teilbar(zahl):
    if zahl % 3 == 0:
        return True
    else:
        return False

def zufaellige_zahl_zw_23_42():
    zahl = random.randint(23, 42)
    while not test3teilbar(zahl):
        zahl = random.randint(23, 42)
    return zahl

# print(zufaellige_zahl_zw_23_42())


# 12 15 18 21
# 4   5  6  7

# 10
# 12
# 4

def zufaellige_zahl_zw_23_42_2():
    zahl = random.randint(8, 14) * 3
    return zahl

def rand_bereich_teilbar_direkt(von=23, bis=42, teiler=3):
    # Wie komme ich von der 23 auf die 8?
    # Bei 12 wäre es ja die 4
#     if von % 3 == 1:
#         von = von + 2
#     elif von % 3 == 2:
#         von = von + 1
#     untere_grenze = von / 3

    if von % teiler == 0:
        rest = teiler
    else:
        rest = von % teiler
    untere_grenze = (von + (teiler - rest)) / teiler

    rest = bis % teiler
    obere_grenze = bis // teiler

    zahl = random.randint(untere_grenze, obere_grenze) * teiler
    return zahl

for i in range(20):
    print(rand_bereich_teilbar_direkt())
```
```python
for i in range(11):
    print(i*2)

for i in range(0, 21, 2):
    print(i)

counter = 0
while counter <= 20:
    print(counter)
    counter = counter + 2

for i in range(0, 21):
    if i % 2 == 0:
        print(i)

for i in range(0, 21):
#     if i % 2 == 0:
    if not i%2:
        print(i)


print(not 1)
```
```python
d = {"Alex": 23, "Sylvia": 12, "Marin": 20, "Eckhart":10, "Charlie": 89}
#
# Hallo, Alex!
# Hallo, Sylvia!...

for dings in d:
    print("Hallo " + dings + "!")
```
```python


# Bittebitte! => Nein.
# Bittebitte! => Nein.
# Bittebitte! => uneaitr
# Bittebitte! => Na gut!
#
# Danke!

# for => definierte anzahl an Ausführungen: Wertevorrat oder Zahlen
#
# while => "solange", unklare anzahl an Ausführungen,
# Solange ausführen, bis ein bestimmter Sachverhalt eintrifft



while True:
    antwort = input("Bittebitte!")
    if antwort == "Na gut!":
        print("Danke!")
        break



antwort = ""
while antwort != "Na gut!":
    antwort = input("Bittebitte!")
print("Danke!")
```


Code aus dem Video
```
import random

23 42

24, 27, 30, 33, 36, 39, 42
 8   9  10  11  12  13  14 => random.randint(8, 14) * 3

Wir haben: 23 und 44
wir wollen: 8 und 14



23 42			aber Teiler: 5


25, 30, 35, 40, 45
 5   6   7   8   9          => random.randint(5, 8) * 5

4.6 => 5
4.2 => 5
4.0 => 4
8.4  => 9


40 => 8  (8.0)
41 => 8  (8.2)
42 => 8  (8.4)
43
44 => 8  (8.8)
45 => 9  (9.0)


def aufrunden(zahl):  # 5.5					8.3  => 9
    i = int(zahl)     # 5					8
    if zahl == i:     # 5.5 == 5 => False
        return i
    else:
        return i + 1



while zahl % teiler != 0:
    zahl = zahl + 1
untere_grenze = zahl/teiler
```

```python
import random



def aufrunden(zahl):  # 5.5					8.3  => 9
    i = int(zahl)     # 5					8
    if zahl == i:     # 5.5 == 5 => False
        return i
    else:
        return i + 1

def zufallszahl_zwischen_mit_teiler(untergrenze=23, obergrenze=42, teiler=3):
    unterer_faktor = aufrunden(untergrenze/teiler)
    oberer_faktor = int(obergrenze/teiler)
    zufallszahl = random.randint(unterer_faktor, oberer_faktor)
    ergebnis = zufallszahl * teiler
    return ergebnis

for i in range(20):
    print(zufallszahl_zwischen_mit_teiler(23, 44, 5))
```