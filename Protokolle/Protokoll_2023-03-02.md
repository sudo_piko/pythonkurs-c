# Pikos Python Kurs am 02.03.2023

## Videos

Hausaufgabenbesprechung:  
- 1: Funktionen: https://diode.zone/w/u6cmPPQLFfUedK6TiQkYN2
- 2: Temperaturskalen übersetzen: https://diode.zone/w/shh1ZuQEvdvGKcV4ZUAD5b
- 3: Unregelmäßige Kreise: https://diode.zone/w/mvnn5kuyCjH2AeoBYw9EZQ

Inhalte:  
https://diode.zone/w/9691EDN2a18ptn84kSTRnY



## Pikos Datei
```python
# from turtle import *
i = 3455
f = 2.3
b = True  # False
n = None

# zusammengesetzte Datentypen 
s = "hallo"
l = [1, 2, 3]
t = (1, 2, 3)

# dictionary
d = {"rot": "red", "grün": "green", "blau": "blue"}
#    key  : value

print(d["grün"])  # => 'green'
# print(l[1])

farbe_deutsch = input("Welche Farbe soll das Dreieck haben? rot, grün oder blau? ")
farbe_englisch = d[farbe_deutsch]
color(farbe_englisch)

# stattdessen in einer Zeile
# color(d[input("??? rgb?")])
circle(200, 360, 3)
```


```python
fahrradkeller = {"Hollandräder": 15, "Lastenräder": 5, "Skateboards":3}

print(fahrradkeller)

# Hollandrad kommt dazu
print(fahrradkeller["Hollandräder"])
# counter = 0
fahrradkeller["Hollandräder"] = fahrradkeller["Hollandräder"] + 1
print(fahrradkeller["Hollandräder"])

# Skateboard kommt weg
print(fahrradkeller["Skateboards"])
fahrradkeller["Skateboards"] = fahrradkeller["Skateboards"] - 1
fahrradkeller["Skateboards"] =                   3          - 1
fahrradkeller["Skateboards"] =  2

# Lastenräder nochmal genauer gezählt...
fahrradkeller["Lastenräder"] = 3

fahrradkeller["Tretroller"] = 20
print(fahrradkeller)

fahrradkeller["Mäuse"] = 1
print(fahrradkeller)


print(fahrradkeller.get("Hollandräder", "keinuer"))


objekt = input("Welches Objekt soll ich zählen? ")
anzahl = fahrradkeller.get(objekt, "keine")
print("Es sind", anzahl, objekt, "im Fahrradkeller.")
```