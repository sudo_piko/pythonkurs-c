# Pikos Python Kurs am 09.03.2023

## Links
https://www.w3schools.com/python/python_dictionaries_methods.asp

## Protokoll
~~~
Protokoll
# mehr eine Zusammenfassung von Notizen...

Zweite Hausaufgabe - Dictionary

alphabet = "abcdefghijklmnopqrstuvwxyz"
zweimal = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"

code = {}

for buchstabe in alphabet:
    stelle_des_buchstaben = alphabet.find(buchstabe)
    stelle_des_neuen_buchstaben = stelle_des_buchstaben + 10
    neuer_buchstabe = zweimal[stelle_des_neuen_buchstaben]
    code[buchstabe] = neuer_buchstabe
    
#print(code)

hello = "hello"
text = "hello world"
text2 = "Hello World"

neues_wort = ""

for buchstabe in hello:
    neuer_buchstabe = code[buchstabe]
    neues_wort = neues_wort + neuer_buchstabe
print(neues_wort)

# funktioniert noch nicht:
# for buchstabe in text:
    # neuer_buchstabe = code[buchstabe]
    # neues_wort = neues_wort + neuer_buchstabe
# print(neues_wort)

# gibt einenen String error, da die Leerstelle nicht erkannt wird. Daher müssen wir mit .get einen neuen Defaultwert setzen

for buchstabe in text:
    neuer_buchstabe = code.get(buchstabe, buchstabe)
    neues_wort = neues_wort + neuer_buchstabe
print(neues_wort)

# nur Kleinbuchstaben erzwingen
for buchstabe in text2.lower:
    neuer_buchstabe = code.get(buchstabe, buchstabe)
    neues_wort = neues_wort + neuer_buchstabe
print(neues_wort)



<---->

Erste Übungsaufgabe - ?

dings = (("Muffin", "Schokolade"), ("Torte", "Erdbeeren"))

# Macht aus "Dings" ein Dictionary und ordnet Muffin --> Schokolade zu und Torte --> Erdbeeren
ein_dictionary = dict(dings)

print(ein_dictionary)

# Benennung ohne Klammern
ein_dictionary_f = dict(Lastenräder=3, Mountainbikes=5)

# über Pop entferne ich ein Element aus dem Dictionary --> Ich verändere das Objekt
a = ein_dictionary_f.pop("Mountainbikes")

print(ein_dictionary_f)


<----->


Methods
# print(...) = Funktionen --> hier muss noch mal ein Argument übergeben werden, weil ja sonst nicht gewusst wird, was geprinted wird
# --> Funktionen im engeren Sinne (functions)

# dings.pop(...) = Methode --> Methode pop wird auf das Objekt Dings ausgeführt
# --> Funktionen im weiteren Sinne = Methoden (methods)
# auch Strings oder Listen haben Methoden
# l = ["a", "b"]
# l.append("c")
# s = "hallo"
# s.find('a')

# viele Methoden verändern ihre Elternobjekte (append, pop), aber nicht alle
# Dictionary Methods: https://www.w3schools.com/python/python_dictionaries_methods.asp
# Website ist ggf. mit vorsicht zu genießen, aber die Methodenlisten sind ganz gut und übersichtlich
# Mit Methoden kann ich for-Schleifen sparen

# random.randint(1,6) = Module

# Warum dict.copy und nicht dict1 = dict2
# dict1 ist keine exakte Kopie von dict2, sondern eher so etwas wie ein "Spitzname", d.h. ich würde auch alle Veränderungen mit verändern
# das passiert bei allen veränderbaren Daten (dictionaries, listen); bei strings würde es mit = funktionieren

# string methods return new values (d.h., sie haben ein "return" dabei) und müssen daher erst noch mit einer Variable initialisiert werden

s = "hallo welt"
z = s.count("l")
# s.count zählt alle "l" in meinem string "l"

print(z)

<----->

Zweite Übungsaufgabe - Spiel

# Aufgabe 1: Baue ein Dictionary mit Spielern, denen Punkte zugeordnet werden

# Variante 1
d = {"Spieler1":25, "Spieler2":28, "Spieler3":137, "Spieler4":5, "Spieler6":83}

# Variante 2
#d = dict(Spieler1=25, "Spieler2":28, "Spieler3":137, "Spieler4":5, "Spieler6":83)

# Aufgabe 2: Gebe die Punktestände aus (mein Versuch)

spieler = input("Welchen Punktestand soll ich ausgeben? ")
punktestand = ""

for i in spieler:
   punktestand = d.get(i)
    
print(key "hat ", punktestand "Punkte".)

----

punktezahlen = {"Alex":5, "Maxi":3, "Jascha":0, "Robin":1}

for name in punktezahlen:
    print(name)
    # gibt mir den key aus --> aber wie komme ich vom key zum value?
    
    print(punktezahlen[namen])
    # gibt alle Punktestände für alle Namen aus
    
    ausgabesatz = name + "hat" + "strg(punktezahlen[name]) + "Punkte"."
    print(ausgabesatz)
    
-----

# Aufgabe 3: Herausfinden, wer die höchste Punktezahl hat und diese ausgeben (mein Versuch)

# Lösung in Worten: Gehe das dictionary durch und gebe mir mit Hilfe einer Methode den key mit dem höchsten value aus

------

# Ziel: den höchsten value finden
# mit values() schauen, wo der höchste value ist und dann den entsprechenden key ausgeben

k = list(punktezahlen.keys()) # Liste
v = list(punktezahlen.values())

print(max(v))
# damit bekommen wir 5 raus, aber wir wollen ja den Index von 5

hoechste_punktzahl = max((v))
stelle_hoechste_punktzahl = v.index(hoechste_punktzahl)

print(stelle_hoechste_punktzahl)

print(k[stelle_hoechste_punktzahl], "hat gewonnen.")

# vorherige Zeilen in einer Zeile komprimiert
gewinner = list(punktezahlen.keys())[v.index(max(list(punktezahlen.values())))]
# gewinner = k						[stelle_hoechste_punktzahl]
# gewinner = k						[v.index(hoechste_punktzahl)]
# gewinner = k						[v.index(max(v))
# k wird ersetzt durch k und v durch v und dann kommt man auf die obere Zeile
print(gewinner)
~~~






## Pikos Datei
```python
# {"a": "k", "b": "l", "c": "m", ...}

alphabet = "abcdefghijklmnopqrstuvwxyz"
zweimal = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"

code = {}

for buchstabe in alphabet:
    stelle_des_buchstaben = alphabet.find(buchstabe)
    stelle_des_neuen_buchstaben = stelle_des_buchstaben + 10
    neuer_buchstabe = zweimal[stelle_des_neuen_buchstaben]
    code[buchstabe] = neuer_buchstabe
    
print(code)

hello = "hello"
text = "hello world!"
text2 = "Hallo Welt!".lower()


neues_wort = ""
for buchstabe in text:
    neuer_buchstabe = code.get(buchstabe, buchstabe)
    neues_wort = neues_wort + neuer_buchstabe
print(neues_wort)
```
```python
f = {"l": 3, "mountainbikes":2}

print(f["mountainbikes"])
```
```python
dings = [("Muffin", "Schokolade"), ("Torte", "Erdbeeren")]

ein_dictionary = dict(Lastenräder=3, Mountainbikes=5)
zweites_dictionary = ein_dictionary#.copy()
print(ein_dictionary)
print(zweites_dictionary)

a = ein_dictionary.pop("Mountainbikes")

print(ein_dictionary)
print(zweites_dictionary)
```
```python
    Funktionen im weiteren Sinne
    functions

Funktionen				Methoden
functions				methods

print(...)				dings.pop(...)
random.randint(1,6)



>>> %Run -c $EDITOR_CONTENT
{'Lastenräder': 3} 5
>>> "UERNTEUTGNDUAERNUAEI".lower()
'uernteutgnduaernuaei'
>>> l = ["a", "b"]
>>> l.append("c")
>>> l
['a', 'b', 'c']
>>> s = "euang"
>>> s.find('u')
1
>>> s
'euang'
>>>
```
```python

# key:value
punktezahlen = {"Alex":5, "Maxi":3, "Jascha":10, "Robin":1}
# oder punktezahlen = dict(Alex=5, Maxi=3,...

for name in punktezahlen:
#     # Uh, was macht das eigentlich? gibt mir das nur die keys? oder nur die values? oder paare?
#     # erstmal rausfinden:
#     print(name)
#     # a-HA! nur die keys. wie komme ich an die values?
#     # Über das dictionary!
#     print(punktezahlen[name])  # das ist ja dann punktezahlen["Alex"], dann punktezahlen["Maxi"], ....
    ausgabesatz = name + " hat " + str(punktezahlen[name]) + " Punkte."
    print(ausgabesatz)
    
k = list(punktezahlen.keys())
v = list(punktezahlen.values())
hoechste_punktzahl = max(v)
stelle_hoechste_punktzahl = v.index(hoechste_punktzahl)
print(k[stelle_hoechste_punktzahl], "hat gewonnen.")

# vorherige Zeilen in einer Zeile komprimiert:
# gewinny = list(punktezahlen.keys())[v.index(max(list(punktezahlen.values())))]
# print(gewinny)
```
```python

punktezahlen = {"Alex":5, "Maxi":3, "Jascha":10, "Robin":1}
gewinner = list(punktezahlen.keys())[list(punktezahlen.values()).index(max(list(punktezahlen.values())))]
```

```python
punktezahlen = {"Alex":5, "Maxi":3, "Jascha":10, "Robin":1}

for name in punktezahlen:
    ausgabesatz = name + " hat " + str(punktezahlen[name]) + " Punkte."
    print(ausgabesatz)
    
k = list(punktezahlen.keys())
v = list(punktezahlen.values())
hoechste_punktzahl = max(v)
stelle_hoechste_punktzahl = v.index(hoechste_punktzahl)
print(k[stelle_hoechste_punktzahl], "hat gewonnen.")
```


## Nachtrag: Code zum Hausaufgabenbesprechungsvideo Obstschale

https://diode.zone/w/vVMSJcdk6nFczhgvqwoU2M
(bester Teil: 9:10 bis 9:18 :D)

```python
obstschale = {"äpfel":4, "orangen":10, "bananen":4, "pfirsiche":6, "melonen":1}
# 
dings = obstschale.items()
print(dings)
print(type(list(dings[0])))
 
# obstschale.values() macht.
# was obstschale.items() macht.
# was obstschale.pop() macht.
# Schreibt mit .get() eine for-Schleife, die ausgibt, wie viele Äpfel, Birnen, Orangen und Bananen in der Schale sind.

for k in obstschale:
#     print(k)
    print(k, ": ", obstschale[k])

# obstschale["äpfel"]
# 4

anfrage = ["äpfel", "birnen", "orangen", "bananen"]

for obst in anfrage:
    anzahl = obstschale.get(obst, "keine")
    print(f"Es sind {anzahl} {obst} in der Obstschale.")
```