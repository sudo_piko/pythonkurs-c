# Pikos Python Kurs am 20.04.2023
NUMMER 1
```python
from random import randint, choice
from turtle import *
import string

bereits_versucht = ["A", "S", "R"]

farben = [(1,0,0), (0,1,1), (1,1,0)]
        	#red
        	#r g b
def zufallskoordinaten():
    koordinaten = (randint(-300, 300), randint(-200, 200))
    return koordinaten

def buchstabe_zu_dreiertupel(buchstabe):
    zahl = string.ascii_uppercase.index(buchstabe)
    dreiertupel = zahl_zu_dreiertupel(zahl)
    return dreiertupel

def zahl_zu_dreiertupel(zahl):
    x = zahl%3
    y = zahl//3 % 3
    z = ((zahl//3) // 3) % 3
    return (x+0.5)/3, (y+0.5)/3, (z+0.5)/3

for buchstabe in bereits_versucht:
    k = zufallskoordinaten()
    pu()
    goto(k)
    color(buchstabe_zu_dreiertupel(buchstabe))

    write(buchstabe, font=("Times New Roman", 200, "bold"))
```
NUMMER 3

```python
def addiere(zahl1, zahl2):
    ergebnis = zahl1 + zahl2
    return ergebnis

def multipliziere(zahl1, zahl2):
    ergebnis = zahl1 * zahl2
    return ergebnis

def quadrat(zahl1, zahl2):
    summe = addiere(zahl1, zahl2)
    ergebnis = summe ** 2
    return ergebnis

def rechnerei(zahl1, zahl2):
    zahl3 = quadrat(zahl1, zahl2)
    ergebnis = zahl3 * 3
    return ergebnis

print(rechnerei(2,3))
```


- Debug rum probieren




## Pads



## Pikos Datei
Hausaufgabenbesprechung
```python
from random import randint, choice
from turtle import *
import string

bereits_versucht = ["A", "S", "R", "Y", "C", "K", "T", "B", "N", "D"]

farben = [(1, 0, 0), (1, 1, 0), (0, 1, 1), (0.5, 0.5, 0.1), (0.1, 0.3, 1)]
#           r  g  b

def zufallskoordinaten():
    koordinaten = (randint(-300, 300), randint(-200, 200))
    return koordinaten

# Soll dann so angewendet werden:
def zahl_zu_dreiertupel(zahl):
    x = zahl%3
    y = zahl//3 % 3
    z = ((zahl//3) // 3) % 3
    return (x+0.5)/3, (y+0.5)/3, (z+0.5)/3

def buchstabe_zu_dreiertupel(buchstabe):
    zahl = string.ascii_uppercase.index(buchstabe)
    dreiertupel = zahl_zu_dreiertupel(zahl)
    return dreiertupel


for buchstabe in bereits_versucht:
    k = zufallskoordinaten()
    pu()
    goto(k)
    color(buchstabe_zu_dreiertupel(buchstabe))
    write(buchstabe, font=("Times New Roman", 200, "bold")) #aaaah!
```

```python


def addiere(zahl1, zahl2):
    ergebnis = zahl1 + zahl2
    return ergebnis

# print(addiere(5, 8))

# Verwendet diese Funktion in einer weiteren Funktion, die zwei Zahlen addiert und das Quadrat zurückgibt
# (also zB 2 + 3 = 5, 5² = 25, also sollte tolle_funktion(2, 3) die Zahl 25 zurückgeben).
def addieren_und_quadrieren(zahl1, zahl2):
    summe = addiere(zahl1, zahl2)
    ergebnis = summe ** 2
    return ergebnis


def rechnerei(zahl1, zahl2):
    zahl3 = addieren_und_quadrieren(zahl1, zahl2)
    ergebnis = zahl3 * 3
    return ergebnis

print(rechnerei(2, 3))
```

Code aus dem Video
```python
punktestand = {"Anna": 5,
               "Berta": 0,
               "Cäsar": 2}
# 				key  : value

# print(punktestand["Anna"])

# for spiely in punktestand:
#     print(spiely)


# print(punktestand["Alex"])
# print(punktestand.get("Alex", 0))


# print(punktestand.get("Berta"))
# punktestand["Berta"] = punktestand.get("Berta") + 1
# print(punktestand.get("Berta"))

punktestand["Daniele"] = 0
# print(punktestand)

# for spiely in punktestand:
#     print(spiely, punktestand.get(spiely, 0))

# for zweiertupel in punktestand.items():
#     print(zweiertupel[0], ":", zweiertupel[1])
#
# for spiely, punkte in punktestand.items():
#     print(spiely, ":", punkte)
```
```python
personen = {"Alexis": {"Alter": 23, "Hobbys": ["Schwimmen", "Tanzen"]},
            "Barbara": {"Alter": 19, "Hobbys": ["Lesen", "Schreiben"]},
            "Can": {"Alter": 25, "Hobbys": ["Kochen", "Reisen"]}}
# 			key   :  value

# print(          personen["Alexis"]                    ["Hobbys"][1])
# print({"Alter": 23, "Hobbys": ["Schwimmen", "Tanzen"]}["Hobbys"][1])
# print(             ["Schwimmen", "Tanzen"]                      [1])
# print(                                       "Tanzen"              )


print(personen["Can"]["Hobbys"][0])
```