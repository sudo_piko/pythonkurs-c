# Pikos Python Kurs am 

## Protokoll
Pythonkurs am 16.03.2023\
Protokoll von Annemarie

-   Hausaufgabenbesprechung:

    -   Aufgabe 2, Fehlercode (siehe Pikos Skript):

        -   Erste Forschleife „in" statt „if"

        -   IndentationError: Einrückungsfehler (Einrückung sind 4
            Leerzeichen, aber anscheinend akzeptiert thonny auch
            weniger, zB 3 Leerzeichen, vielen Dank an thonny. Aber
            besser ist es, immer mit einem Tab einzurücken, dann
            konvertiert die IDE es in die jeweilig gebrauchte
            Einrückung)

        -   An einer Stelle im Code erkennt python „dict" als
            funktion/befehl, nicht als Teil des von uns vergebenen
            Variablennamen

        -   Drei Anführungszeichen um einen string machen, dass der
            string in sich Leerzeichen und Umbrüche haben kann. Mit nur
            einem Anführungszeichen akzeptiert python das nicht.

        -   Ein dictionary braucht geschweifte Klammern, eine funktion
            braucht Klammer-auf-klammer-zu hinter sich

        -   text.split() spaltet einen string in Bestandteile auf und
            packt alles in eine Liste

        -   If wort in woerter_dict ist aus der Überlegung entstanden,
            dass erst geschaut werden soll, ob ein Wort in der Liste
            drin ist. Aber woerter_dict.get() macht es über .get()
            direkt, ohne if, also schneller

        -   Ein doppeltes == bedeutet für den Computer eine Frage (ist a
            == gleich b?), bei einer Zuweisung brauchen wir nur ein =

        -   das f vor den Anführungszeichen in
            print(f"...{variablenname}.") macht, dass innerhalb der
            Anführungszeichen der Inhalt einer Variable eingefügt wird.
            Bei einer forschleife get print dann die Elemente des
            dictionaries durch.

    -   Aufgabe 1, Spiel (Pikos Skript):

        -   Erstmal Input - wer will überhaupt mitspielen? Am besten so
            lange nach Namen fragen, bis keine weiteren mehr eingetragen
            werden. Also, solange der Input nicht leer ist -- also:
            WHILE schleife! Genauso wie eine if bedingung, braucht while
            eine Kondition am Anfang, die true oder false sein muss.
            While kann 1 zu 1 mit „solange" übersetzt werden. Es ist
            fast wie eine for-schleife, aber ist an eine if-Bedingung
            gebunden, die Schleife läuft so lange weiter wie diese TRUE
            ist.

        -   != bedeutet „ungleich", == bedeutet „genau gleich" (!
            Ausrufungszeichen negiert das folgende zeichen/bedingung)

        -   None ist nicht gleich nichts, „leerer Input" wird hier
            stattdessen als „" definiert

        -   „break" an irgendeiner Stelle im Code beendet das Programm,
            kann zum Beispiel in einer if bedingung innerhalb einer
            Schleife eingebaut werden. While True: ... dann „break" im
            Code sieht man häufig, weil die Schleife dann mitten drin
            abgebrochen wird (da wo break steht) und nicht erst zu Ende
            durchläuft

        -   Die einfachste Art random zu importieren, wenn nur eine
            Funktion davon gebraucht wird, ist random.ranint(1,6) (ein
            Würfel), anstatt oben im Code „from random import randint"
            zu schreiben

        -   Vorschlag auf dem Chat um das Gewinni aus dem dict
            auszugeben: gewinner = max(spieler_dict, key =
            spieler_dict.get), aber das ist fortgeschrittene Dunkelheit

        -   Was passiert bei Gleichstand? Die erste Person als Gewinni
            wird ausgewählt, nicht beide. Das ist unfair. Was tun? --
            das wird unsere Hausaufgabe

        -   Bei verbliebenen Fragen zum Spiel / zur Hausaufgabe:
            schreibt ne Mail an Piko


-   Dictionary Aufgabe (Pikos Skript):

    -   Schreibt ein dictionary, und dann eine Dreieckfunktion, die die
        werte aus dem dict ausliest

    -   Das Wichtigste, was eins sich bei dicts merken muss: wie kommt
        eins an die keys? in der forschleife geht der iterator (counter)
        die keys durch, hier mit dem Namen „dings". Wie kommt eins an
        die values? Dreieck(d\[dings\]) in pikos skript

    -   Unterschied d\[dings\] und d.get(dings) ist, dass .get()
        stabiler ist. Wenn das gesuchte Element dings nicht im dict
        vorhanden ist, dann gibt d\[dings\] eine Fehlermeldung aus, bei
        .get(dings) allerdings nicht.


-   Nächstes Mal Unterricht fällt aus (23.03.2023), evtl auch
    übernächste Woche (30.03.2023). Ankündigungen und die
    unterrichtsersetzenden Videos gibt es im gitlab. Treffen in den
    Kleingruppen sollen trotzdem weiter stattfinden. Wenn die Gruppen zu
    klein werden, sucht euch über den Chat neue

-   Bis über oder überübernächste Woche!



## Pikos Datei
### Hausaufgaben
Aufgabe 2
```python
text = """Everyone has the right to live by the River Vilnele, and the River Vilnele has the right to flow by everyone.
Everyone has the right to hot water, heating in winter and a tiled roof.
Everyone has the right to die, but this is not an obligation.
Everyone has the right to make mistakes mistakes.
Everyone has the right to be unique."""

woerter_dict = {}

for wort in text.split():
    woerter_dict[wort] = woerter_dict.get(wort, 0) + 1

print(woerter_dict)  # {"Everyone":5, "has":6, ...

print(max(woerter_dict))
for wort in woerter_dict:
    print(f"Das Wort {wort} kommt {woerter_dict[wort]} mal vor.")  # Diese Zeile ist korrekt! So kann eins Variablen 
    # ohne + in einen String einfügen.
```
Mehrzeilige Strings
```python
text = "hallo"

text2 = """hallo
hallo"""

print(text2)
```
String-Methode split()
```python
text = """Everyone has the right to live by the River Vilnele, and the River Vilnele has the right to flow by everyone.
Everyone has the right to hot water, heating in winter and a tiled roof.
Everyone has the right to die, but this is not an obligation.
Everyone has the right to make mistakes.
Everyone has the right to be unique."""


dings = text.split()

print(dings)
print(type(dings))
```
Welches Problem löst get()?
```python
obstschale = {"apfel":3, "bananene":2}

print(obstschale["kirschen"])
```
f-Strings
```python
name = "Piko"

begruessung = f"Hallo, {name}, schön dass du da bist!"
begruessung = "Hallo, " + name + ", schön dass du da bist!"

print(begruessung)
```
Aufgabe 1: Spiel
```python
import random

RUNDENZAHL = 2

antwort = None
punktezahlen = {}

while antwort != "":
    antwort = input("Gib den Namen der nächsten Spielenden Person ein: ")
    if antwort != "":
        punktezahlen[antwort] = 0
    
# while True:
#     antwort = input("Gib den Namen der nächsten Spielenden Person ein: ")
#     if antwort == "":
#         break
#     punktezahlen[antwort] = 0

print(punktezahlen)

for i in range(RUNDENZAHL):
    print(f"Runde {i+1}")
    bisheriges_gewinny = ("", 0)
    for person in punktezahlen:
        wurf = random.randint(1, 6)
        if wurf > bisheriges_gewinny[1]:
            bisheriges_gewinny = (person, wurf)
        
        print(f"    {person} hat {wurf} gewürfelt.")
    print(bisheriges_gewinny)
    punktezahlen[bisheriges_gewinny[0]] = punktezahlen[bisheriges_gewinny[0]] +1 
    
    # => Hausaufgabe

    

# punktezahlen = {"Alex":5, "Maxi":3, "Jascha":10, "Robin":1}

for name in punktezahlen:
    ausgabesatz = name + " hat " + str(punktezahlen[name]) + " Punkte."
    print(ausgabesatz)
    
k = list(punktezahlen.keys())
v = list(punktezahlen.values())
hoechste_punktzahl = max(v)
stelle_hoechste_punktzahl = v.index(hoechste_punktzahl)
print(k[stelle_hoechste_punktzahl], "hat gewonnen.")
```
while-Schleifen
```python
counter = 0

while counter < 10:
    print(counter)
    if counter ** 2 == 16:
        counter = 6
    counter = counter + 1
```
Dictionaries mit Turtle
```python
from turtle import *

d = {"red": 30, "blue":60, "yellow":90}

dings = "green"
d[dings]
d.get(dings)
# => malt ein kleines rotes, ein mittelgroßes blaues und
# ein großes gelben dreieck mit diesem dictionary und einer for-Schleife

def dreieck(l):
    for i in range(3):
        fd(l)
        lt(120)

pensize(10)

for dings in d:
    print(dings)
    print(d[dings])
    
    color(dings)
    dreieck(d[dings])
    pu()
    fd(100)
    pd()
```