# Pikos Python Kurs am 09.02.2023

Neue Videos zum Thema Flächen: 
https://diode.zone/w/hXfudXPVqyqq68RNPc7RQM
https://diode.zone/w/c7ZCYQcjHpcg1wACjuw7fK
# Protokoll

pos() - speichert die aktuelle position der turtle

setheading(0) – setzt die turtle in ausgangsrichtung

Standardwerte bzw. Defaultargumente in Funktionen immer als letztes angeben.

%- (Modulo) ermittelt den Rest bei der Division der ersten Zahl durch die zweite



## Ressourcen

Im Abbildungen-Ordner findet Ihr die Datei "Piko_Bibliotheken.jpg", die Ihr als Spickzettel für die verschiedenen 
Import-Möglichkeiten verwenden könnt.
![](../Abbildungen/Piko_Bibliotheken.jpg)

Im Ordner Loesungen findet Ihr die Lösungen zu den Aufgaben.



## Pikos Datei
```python
from turtle import *
from random import randint
from math import sqrt


speed(0)
pensize(5)
def kachel(ecke=0, laenge=100):
    # Kachel
    p = pos()
    pu()
    for i in range(ecke):
        fd(laenge)
        lt(90)
    begin_fill()
    fd(laenge)
    lt(90)
    fd(laenge)
    lt(90)
    end_fill()

    pu()
    goto(p)
    setheading(0)

def zufallkachel(laenge=100):
    kachel(randint(0, 3), laenge)


def eine_kachel(welche_kachel="zufall", laenge=100):
    if welche_kachel == "zufall":
        ...
    if welche_kachel == "ro":
        ...
        
def zufalls_flaeche(hoehe, breite, kachelbreite=100):
    for i in range(hoehe):
        for j in range(breite):
            zufallkachel(kachelbreite)
            fd(kachelbreite)
        pu()
        bk(breite * kachelbreite)
        rt(90)
        fd(kachelbreite)
        lt(90)
        pd()


def kachel_schraege_linien(laenge=100, ausrichtung=0):
    p = pos()
    pu()
    ausrichtung = ausrichtung % 2
    if ausrichtung == 1:
        fd(laenge)
        lt(90)
    lt(45)
    pd()
    fd(laenge*sqrt(2))
    pu()
    lt(135)
    fd(laenge)
    if ausrichtung == 0:
        lt(90)
        fd(laenge)
    lt(90)


def abwechselnd_flaeche(hoehe, breite, kachelbreite=100):
    counter = 0
    for i in range(hoehe):
        for j in range(breite):
#             e = counter % 4
            kachel_schraege_linien(kachelbreite, randint(0,1)) #counter%7)
            fd(kachelbreite)
            counter = counter + 1
        pu()
        bk(breite * kachelbreite)
        rt(90)
        fd(kachelbreite)
        lt(90)
        pd()


abwechselnd_flaeche(5, 6)
```

```python
from turtle import *

speed(0)

def kreiskachel(ausrichtung, seitenlaenge=200):
    pensize(2)
    ausrichtung = ausrichtung % 2
    
    pd()
    for i in range(4):
        fd(seitenlaenge)
        lt(90)
        

    pensize(10)

    penup()
    if ausrichtung == 1:
        fd(seitenlaenge)
        lt(90)
    fd(seitenlaenge/2)
    lt(90)
    pendown()
    circle(-seitenlaenge/2, 90)
    lt(90)
    pu()
    fd(seitenlaenge/2)
    lt(90)
    fd(seitenlaenge/2)
    lt(90)
    pendown()
    circle(-seitenlaenge/2, 90)
    lt(90)
    pu()
    fd(seitenlaenge/2)
    lt(90)
    if ausrichtung == 1:
        rt(90)
        fd(-seitenlaenge)


def abwechselnd_flaeche(hoehe, breite, kachelbreite=100):  #dann passt der Funktionsname nicht mehr...
    counter = 0
    for i in range(hoehe):
        for j in range(breite):
#             e = counter % 4
            kreiskachel(counter%3, kachelbreite)
            fd(kachelbreite)
            counter = counter + 1
        pu()
        bk(breite * kachelbreite)
        rt(90)
        fd(kachelbreite)
        lt(90)
        pd()

abwechselnd_flaeche(5, 7)

```