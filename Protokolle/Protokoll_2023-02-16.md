# Pikos Python Kurs am 

## Hausaufgabenbesprechung
Aufgabe 2:  
https://diode.zone/w/kRPgpbn2AZDuEV6tPLFjAU


## Pikos Datei
Probleme mit Funktionen als Default-Argumente:
```python
from random import randint


# Die Problemfunktion:

def zahlprinten(a=randint(1,20)):
    print(a)

# warum es ein Problem ist:
for i in range(15):
    zahlprinten()

# Bessere Versionen:
def zahlprinten0(a=1):
    print(a)
    
zahlprinten0()

zufallszahl = randint(1,20)
def zahlprinten(a=zufallszahl):
    print(a)

def zahlprinten2(a=None):
    if a == None:
        a = randint(1, 20)
    print(a)

# zahlprinten(5)  # "wrapper" für print

zahlprinten("leer")
zahlprinten2("leer")

# for i in range(15):
#     zahlprinten2()
```
```python
from turtle import *
import random

setup(width=500, height=500, startx=2560, starty=0)
pensize(3)
speed(8)

seitenlaenge = 30
reihenlaenge = 5
hoehe = 3


def quadratkreis(l):
    for i in range(4):
        fd(l)
        lt(90)
    fd(l / 2)
    circle(l / 2)
    fd(-l / 2)

def leeres_q(l):
    for i in range(4):
        fd(l)
        lt(90)

a = quadratkreis(100)
print(a)

for j in range(hoehe):
    for i in range(reihenlaenge):
        goto(i * seitenlaenge, j * seitenlaenge)
        pd()
        r = random.randint(0, 1)
        if r == 1:
            quadratkreis(seitenlaenge)
        else:
            leeres_q(seitenlaenge)
        pu()
```
```python
liste = [1, 2, 3, 4, 6, 7, 8, 9, 10]

s = "eaurtnaeurtnoeartnoeua"
z = str(324958453)
if str(3) in z:
    print("ist drin")
else:
    print("ist nicht drin")
```
```python
from turtle import *
from random import randint

# print(type(None))

ding = None
ding = 5


z = randint(1,20)
print(z)

a = print(12)
print(a)

b = fd(100)
print(b)
```
```python

def addieren(a, b):
    ergebnis = a + b
    return ergebnis
    
    
c = addieren(23, 42)
print("c ist: ", c)


# baut eine Funktion, die eine Zahl zurückgibt, die um 5 höher ist, als
# die Zahl, die reingetan wurde.
def plusfuenf(zahl):
    ergebnis = zahl + 5
    return ergebnis
# Kürzere Version:
def plusfuenf(zahl):
    return zahl + 5

zahl = plusfuenf(4)  # => in zahl ist jetzt 9, wird aber nicht geprintet
print(zahl)  # erst jetzt wird die Zahl geprintet

def quadrat(zahl):
    return zahl * zahl

zahl = quadrat(5)  # => 25
print(zahl)

quadrat(6)  # sowas würde auf der Kommandozeile gleich geprintet, aber im Editor nicht!
4  # auch das hier!

```
```python

```