# Pikos Python Kurs am 18.05.2023

## Pads

Sammlung für Vieleck-Wirbel:
https://md.ha.si/TyTRRZjPSJqAdKp3GZSCIg#



## Pikos Datei

![Pikos](../Abbildungen/Scopes.png)
```python

spielfeld = [[" ", " ", " "],
         [" ", "X", "X"],
         ["O", " ", " "]]

def aktualisiere_spielfeld(spielfeld, x, y, zeichen):
    spielfeld[y][x] = zeichen

aktualisiere_spielfeld(spielfeld, 0, 0, "X")
print(spielfeld)


hello = "Hallo Welt!"

def addiere(x, y):
    z = x + y
    return z

a = addiere(2, 3)
```

Wie komme ich an die Spalten?
```python
import turtle
# Ein Spielfeld mit "p"s in der mittleren Spalte
spielfeld = [["-", "p", " "],
             ["-", "P", "X"],
             ["O", "P", " "]]

# mittlere Spalte per Hand ausgeben lassen:
# print(spielfeld[0][1])
# print(spielfeld[1][1])
# print(spielfeld[2][1])

# mittlere Spalte ausgeben lassen:
for i in range(3):
    print(spielfeld[i][1])

# Alle Spalten ausgeben lassen:
for j in range(3):  
    for i in range(3):
        print(spielfeld[i][j])
```


```python
from random import randint
import time
dir()
dir(__builtins__)
```