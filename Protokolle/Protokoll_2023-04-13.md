# Pikos Python Kurs am 13.04.2023

## Pads
https://md.ha.si/u2NJaOiDT06mebkFTxA_iw#


## Pikos Datei
Unterschied zwischen Funktionen, die nichts zurückgeben und Funktionen, die etwas zurückgeben
```python
def addiere_1(zahl1, zahl2):
    dings = zahl1 + zahl2
    print(dings)



def addiere_2(zahl1, zahl2):
    dings = zahl1 + zahl2
    return dings


dings1 = addiere_1(2, 3)
dings2 = addiere_2(2, 3)
dings3 = dings2 * 2


print("huiuiuiui")

print(dings2)

print('We have a problem: We didn't find the franzbroetchens.')
```

Hangman mit Buchstabenbild
```python
# Hangman
# 27.03.2023
# by piko

# wir brauchen ein Wort
from woerter import dramatiker
from random import choice, randint
import bilder
from turtle import *

FEHLSCHLAEGE_MAX = len(bilder.asciibilder)


wort = choice(dramatiker)
print(wort)
wort = wort.upper()


bereits_versucht = []
fehlschlaege = 0

anzeige = "_" * len(wort)

# große Schleife
while anzeige != wort:
    # buchstabe abfragen
    buchstabe = input("Bitte Buchstabe eingeben! ").upper()
    if len(buchstabe) > 1:
        print("Input war zu lang!")
    elif len(buchstabe) == 0:
        print("Kein Input.")
    else:
        # if buchstabe is alphabetical
        if buchstabe.isalpha():
            # testen, ob buchstabe schonmal vorkam
            if buchstabe in bereits_versucht:
                print("Diesen Buchstaben hatten wir schon!")
            else:
                bereits_versucht.append(buchstabe)
                # testen, ob der buchstabe im Wort ist
                if buchstabe in wort:
                    # wenn ja: in die anzeige einfügen
                    for stelle in range(len(wort)):
                        if wort[stelle] == buchstabe:
                            anzeige = anzeige[:stelle] + buchstabe + anzeige[stelle+1:]
                    print(anzeige)
                else:
                    # wenn nein: fehlschlag-counter erhöhen
                    print("Dieser Buchstabe kommt nicht im Wort vor.")
                    print(bilder.asciibilder[fehlschlaege])
                    fehlschlaege = fehlschlaege + 1
                    print(f"Fehlschläge: {fehlschlaege}")
                    # wenn fehlschläge zu hoch: abbrechen;
                    if fehlschlaege > FEHLSCHLAEGE_MAX:
                        print("Zu viele Fehlversuche!")
                        break
        else:
            print("Kein Buchstabe!")

# lösung zeigen
print(wort)
# zeigen, wie viele Fehlschläge



print(bereits_versucht)
for buchstabe in bereits_versucht:
    pu()
    goto(randint(-300, 300), randint(-300, 300))
    write(buchstabe, move=False, align="left", font=("Arial", 30, "normal"))
```

Hangman mit Buchstabenbild und bunten Kreisen
```python
from turtle import *
from random import randint
speed(0)
bereits_versucht = ['A', 'E', 'S', 'C', 'H', 'Y', 'L', 'O', 'U']

# while len(bereits_versucht) < 100:
#     bereits_versucht = bereits_versucht + bereits_versucht
# print(bereits_versucht)
# for buchstabe in bereits_versucht:
#     pu()
#     goto(randint(-300, 300), randint(-300, 300))
#     write(buchstabe, move=False, align="left", font=("Arial", 100, "normal"))


def zahl_zu_dreiertupel(zahl):
    x = zahl%3
    y = zahl//3 % 3
    z = ((zahl//3) // 3) % 3
    return (x+0.5)/3, (y+0.5)/3, (z+0.5)/3

pensize(20)
for buchstabe in bereits_versucht:
    zahl_des_buchstabens = ord(buchstabe) - 65
    dreiertupel = zahl_zu_dreiertupel(zahl_des_buchstabens)
    print(dreiertupel)
    color(dreiertupel)
    pu()
    goto(randint(-300, 300), randint(-300, 300))
    pd()
    circle(100)
```

Farbenratespiel, quick and dirty
```python
from random import random
from turtle import *

farbe = (round(random(), 2),
         round(random(), 2),
         round(random(), 2))

color(farbe)
pensize(200)
dot()

input()

print(farbe)
```