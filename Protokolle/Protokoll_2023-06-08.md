# Pikos Python Kurs am 08.06.2023

## Pads
Wie wollen wir weitermachen?  
https://md.ha.si/YK1i-RkHSDSiT-7LaJsADQ?view

## Hausaufgabe
Eine Beispiellösung findet Ihr in Projekte/pygame/04_pygame_Tasten.py

## Pikos Datei
Voronoi-Diagramme!  
Vorgehen:
```python
# Screengrenzen definieren


# Punkte verteilen
# Punkte: x-Koordinate, y-Koordinate, Farbe

# Alle "Pixel" durchgehen: wahrscheinlich eine for-Schleife
    # bei jedem schauen, welcher von den Punkten der nächste ist:
        # für alle Punkte rausfinden, ob der Pixel, der uns gerade interessiert, der nähcste ist.
            # distanz messen, schauen ob das kürzer ist, als die bisher kürzeste Distanz.
            # wenn ja:
                # dann ist das die neue kürzeste distanz
            # wenn nein:
                # dann bleibt die alte kürzeste distanz korrekt
        # Pixel bekommt die farbe von dem Gewinnerpunkt.
```

```python
# Punkte: x-Koordinate, y-Koordinate, Farbe

punkt1_koordinate = (2, 5)
punkt1_farbe = (0, 0, 1)

punkt1 = (punkt1_koordinate, punkt1_farbe)

# x-Koordinate
print(punkt1[0][0])  # => 2
# print(punkt1.x)  # wäre viel schöner!
# Farbe
print(punkt1[1])  # => (0, 0, 1)
# print(punkt1.farbe)  # wäre viel schöner!
```

```python
# Klasse ~ Typ
class Punkt:
    def __init__(self, x, y, farbe):
        self.x = x
        self.y = y
        self.farbe = farbe
        
    def verschieben(self, x=1, y=0):
        self.x = self.x + x
        self.y = self.y + y

liste_von_pnukten = []
for i in range(2000):
    liste_von_pnukten.append(Punkt(i, random.randint(0, 100), "red"))

p1 = Punkt(5, 2, "red")
print(p1)
print(type(p1))

print(p1.x, p1.y)
p1.verschieben(x=0, y=200)
        p1.x = p1.x + x
        p1.y = p1.y + y
print(p1.x, p1.y)
```

```python
class Dingsi:
    def __init__(self, dings, dongs):  # double underline => dunder
        self.dings = dings
        self.dongs = dongs

    def dings_erhoehen(self, zahl):
        self.dings = self.dings + zahl

    def gib_dongs_mehrfach(self, zahl):
        ergebnis = self.dongs * zahl
        return ergebnis
    
    
a = Dingsi(2, 5)
a.dings_erhoehen(1)

print(a.dings)


r = a.gib_dongs_mehrfach(3)
print(type(r))
print(r)
```

![Voronoi](../Abbildungen/Klassen_Cheatsheet.png)