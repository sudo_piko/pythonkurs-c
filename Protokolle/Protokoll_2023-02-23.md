# Pikos Python Kurs am 
Pythonkurs am 23.02.2023\
Protokoll von Annemarie

-   Nächste Woche fällt der Kurs aus, aber es gibt ein Video

-   Termine:

    -   3.3.: globaler Klimastreik

    -   Morgen (24.02.2022): noch eine Runde Easterhegg Tickets möglich
  
- Wir können ab jetzt entweder mehr generative Kunst weiter machen,
    oder mehr Spieleprogrammierung (sogar inklusive Ton). Umfrage --
    Spieleprogrammierung 60%, Mehr Kunst 40%

-   Hausaufgabenbesprechung

    -   Aufgabe 1: (Pikos Skript)

        -   ist_durch_3_teilbar: den Modulo-Operator einbauen

        -   wenn in if-Bedingungen oder sonstwo Rechnungen stehen, dann
            wird die Rechnung direkt ausgerechnen und nicht in ihren
            Bestandteilen weiterverarbeitet

        -   Das True False in eine if-else-Bedingung zu schreiben kann
            man sich sparen, wenn man einfach direkt „return zahl%3 ==
            0" als kompletten Funktionskörper definiert. Dann kommt auch
            automatisch True und False zurück.

        -   „==" ist ein Boolean Operator. Dabei kommt am Ende ein True
            oder False raus, deshalb müssen wir keinen Umweg über
            if/else/return true/false machen.

    -   Aufgabe 2: (Pikos Skript)

        -   Das Beispiel ist sehr lebensnah gewählt, es lohnt sich das
            auszurechnen

        -   Beim letzten Mal haben wir erst die Enden der Startrange
            addiert, um die Gesamtrange über 0 zu haben
            (-(-300)+300=600), dann durch denselben Wert (600) geteilt
            um auf eine Zielrange von 0-1 zu kommen. Diese Rechnung kann
            einfach je nach gewünschter Start- und Zielrange umgestellt
            werden.

        -   Es kommt noch ein Erklärvideo dazu

    -   Aufgabe 3: (Pikos Skript)

        -   extendList hängt Dinge an Listen dran -- Listen sind
            veränderbar. Sie „merken" sich dann Sachen und tragen sie
            mit sich rum. Die Listen laufen durch die Welt und verändern
            sich, das Leben prägt sie und uns, sind wir nicht alle bei
            der Geburt leere Listen? Was in die Liste gefüllt wurde,
            bleibt auch darin, sofern es nicht aktiv entfernt wird.
            Tupel und Strings haben diese Eigenschaft nicht, sie sind
            nicht veränderlich.

        -   Das ist eine beliebte Fehlerquelle, merkt euch: „Listen
            machen manchmal komische Dinge."


-   Unterschied tuple und liste: Tuple ist eine nicht veränderliche
    Auflistung. Tuple haben runde, Listen eckige Klammern. Listen sind
    veränderlich. Tinoca steuert ein großartiges Cheatsheet dazu bei:  
    https://gitlab.com/Tinoca/pykurs/-/blob/4a457a07ab2d9839092ac11a6fecaf6139cd9f27/python_strukturen_Sylvia.pdf

-   Aufgabe (Pikos Skript): Funktion definieren, die konvertieren heißt,
    und die einen Farbnamen annimmt und einen Tuple zurückgibt. Man kann
    Klammern um die Farbwerte machen, muss man aber nicht ((0,1,1) hier
    gleich wie 0,1,1). Aber man kann man nicht „farbname" == 0,1,1
    schreiben, weil python sonst rechnet, hier muss eine Klammer um
    (0,1,1) (kleine Erinnerung an den Anfang der Stunde: „wenn in
    if-Bedingungen oder sonstwo Rechnungen stehen, dann wird die
    Rechnung direkt ausgerechnen und nicht in ihren Bestandteilen
    weiterverarbeitet")

-   Aufgabe (Pikos Skript): Funktion definieren, die Zufallskoordinaten
    ausgibt

-   Funfact: Wenn man die Ausführung eines Gesamtskripts unterbrechen
    will, kann man input() dazwischenpacken, dann wird der Code nur bis
    dahin ausgeführt und wartet dann auf input



## Pikos Datei
```python
def ist_durch_drei_teilbar1(zahl):
    if zahl%3 == 0:
        return True
    else:
        return False
    
    
    
z = (3+5) * 7
z =   8   * 7
z =     56

def ist_durch_drei_teilbar(zahl):
    return zahl%3 == 0  # "==" ist ein boolean Operator => am Ende kommt da True oder False raus

    # 
    # 
    # if zahl%3 == 0:
    # if    2   == 0:
    # if    False:
    # 
    #     return True
    # else:
    #     return False



for i in range(15):
    print(i, ist_durch_drei_teilbar(i))
    print(i, i%3 == 0)
```
```python
# -300 bis 300
#   0  bis 600   # 300 addiert
#  0   bis 1     # durch 600 teilen




def rotkanal(zahl):
    zahl = zahl + 300
    zahl = zahl / 600
    return zahl

def rotkanal2(zahl):
    return (zahl + 300) / 600

def skaliere(zahl, ursprungsbereich=(100, 300), zielbereich=(50, 150)):
    zahl = zahl - ursprungsbereich[0]
    zahl = zahl / (ursprungsbereich[1] - ursprungsbereich[0])
    zahl = zahl * (zielbereich[1] - zielbereich[0])
    zahl = zahl + zielbereich[0]    
    return zahl
    

for i in range(-300, 301, 20):
    print(skaliere(i, ursprungsbereich=(100, 300), zielbereich=(50, 150)))
```
```python
def extendList(dings, liste=[]):
    # Diese Funktion soll das Objekt dings an die Liste liste anhängen. Wenn kein liste übergeben wird, soll eine 
    # leere Liste verwendet werden. 
    liste.append(dings)
    return liste
# 
# o = ["apfel", "orange", "mandarine"]
# a = extendList("banane", o)
# print(a)
# print(o)
# 


l1 = extendList(10)  # Was ist l1?
print(l1)
l2 = extendList(123, [l1])  # Was ist l2?
print(l2)
l3 = extendList('a')  # Was sollte l3 sein?
print(l3)
l3 = extendList('a')  # Was sollte l3 sein?
print(l3)
extendList('b')  # Was sollte l3 sein?
print(l3)
print("harald", l1)
print(l2)

# wtf, python?!
```
```python

def m():
    return ["hallo", False]

a = m()
print(type(a))


def konvertieren(farbwort):
    if farbwort == "green":
        return (0, 1, 0)
    elif farbwort == "blue":
        return 0, 0, 1
    elif farbwort == "purple":
        return 0.5, 0, 1
    elif farbwort == "red":
        return 1, 0, 0
    elif farbwort == "yellow":
        return 1, 1, 0

werte = konvertieren("green") # (0, 1, 0)


def konvertieren2(farbtupel=(1,0,0)):
    if farbtupel == 0, 1, 0:
        return "green"
```
```python
import random


def zufallskoordinaten():
    x = random.randint(-300, 300)
    y = random.randint(-200, 200)
    return x, y




k = zufallskoordinaten()  # => k: (23, 442)
print(k)
```