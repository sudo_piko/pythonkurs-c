https://github.com/Quefumas/gensound/wiki/Melodic-Shorthand-Notation# Pikos Python Kurs am 

## Pads



## Pikos Datei
Hausaufgabe
```python
#              C   D   E   F   G   A   B     C    C
diatonisch = [60, 62, 64, 65, 67, 69, 71] # 72   84   96   108   120  
#				   62						74   86   98
# 48 36 24 12 0

c_offset = 0
d_offset = 2
e_offset = 4
f_offset = 5


alle_c = []
for i in range(10):
    alle_c.append(i * 12)
    
# print(list(range(c_offset, 127, 12)))

def midi_werte_von_allen_oktaven_eines_tones(midi_wert=60):
    offset = midi_wert % 12
    return list(range(offset, 127, 12))

def midi_werte_von_allen_oktaven(tonliste):
    ergebnis = []
    for ton in tonliste:
#         ergebnis.append(midi_werte_von_allen_oktaven_eines_tones(ton))  # gibt eine verschachtelte Liste :( [[..], [...]]
        ergebnis += midi_werte_von_allen_oktaven_eines_tones(ton)
    ergebnis.sort()
    return ergebnis

def mit_grenze(tonliste, untere=21, obere=108):
    alle_toene = midi_werte_von_allen_oktaven(tonliste)
    neue_liste = []
    for ton in alle_toene:
#         if ton >= 21 and ton <= 108:
        if 21 <= ton <= 108:
            neue_liste.append(ton)
    return neue_liste
    

print(mit_grenze([60, 62, 64]))   # => [0, 2, 4, 12, 14, 16, 24, 26, 28, ...
```

```python
from gensound import Sine
from random import choices


notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

class Ton:
    def __init__(self, midiwert, length=1):
        self.midiwert = midiwert
        self.length = length

    def string_mit_oktave(self):
        """Gibt den Ton als String mit Oktave zurück, zB "D4" """
        oktave = self.midiwert // 12 - 1  # C0 ist leider nicht MIDI-Wert 0, sondern MIDI-Wert 12...
        tonhoehe = self.midiwert % 12  # Stellung in der Oktave: C=0, C#=1 etc. Demnach wie in der Liste notes oben!
        tonname = notes[tonhoehe]
        return tonname + str(oktave)

    def string_mit_laenge(self):
        """Gibt den Ton als String mit Länge zurück, zB "C4=2" """
        return self.string_mit_oktave() + "=" + str(self.length)
    
diatonic = ['C', 'D', "E", 'F', 'G', 'A', 'B', 'C']
melody1 = " ".join(choices(diatonic, k=10))  # => "C D E...


ton1 = Ton(62)
# s = Sine(ton1.string_mit_oktave(), duration=0.5e3)
# s.play()

midiwerte = [57, 59, 60, 62, 64, 60, 64, 64, 63, 59, 63, 63, 62, 58, 62, 62, 57, 59, 60, 62, 64, 60, 64, 69, 67, 64, 60, 64, 67, 67]
melodieliste = []
for wert in midiwerte:
    ton = Ton(wert)
    melodieliste.append(ton.string_mit_oktave())
    
print(melodieliste)

melody2 = " ".join(melodieliste)

s = Sine(melody2, duration=0.5e3)
s.play()
```

Ideensammlung für die Melodieklasse:
übergeben: Liste von midi-Werten

Attribute:
- ob swingend oder nicht
- stotternd (oder andere Artikulationen, Lautstärke)
- Töneliste
- Geschwindigkeit


Methoden:
- Töne durcheinanderwürfeln
- "als_string": Melodie so ausgeben, dass sie in Sine reingepackt werden kann


```python
from gensound import Sine
from random import choices
notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

class Ton:
    def __init__(self, midiwert, length=1):
        self.midiwert = midiwert
        self.length = length

    def string_mit_oktave(self):
        """Gibt den Ton als String mit Oktave zurück, zB "D4" """
        oktave = self.midiwert // 12 - 1  # C0 ist leider nicht MIDI-Wert 0, sondern MIDI-Wert 12...
        tonhoehe = self.midiwert % 12  # Stellung in der Oktave: C=0, C#=1 etc. Demnach wie in der Liste notes oben!
        tonname = notes[tonhoehe]
        return tonname + str(oktave)

    def string_mit_laenge(self):
        """Gibt den Ton als String mit Länge zurück, zB "C4=2" """
        return self.string_mit_oktave() + "=" + str(self.length)
    
    def frequenz(self):
        return ???

diatonic = ['C', 'D', "E", 'F', 'G', 'A', 'B', 'C']
melody1 = " ".join(choices(diatonic, k=10))  # => "C D E...


# ton1 = Ton(62)
# s = Sine(ton1.string_mit_oktave(), duration=0.5e3)
# s.play()

# midiwerte = [57, 59, 60, 62, 64, 60, 64, 64, 63, 59, 63, 63, 62, 58, 62, 62, 57, 59, 60, 62, 64, 60, 64, 69, 67, 64, 60, 64, 67, 67]
# melodieliste = []
# for wert in midiwerte:
#     ton = Ton(wert)
#    melodieliste.append(ton.string_mit_oktave())
    
# print(melodieliste)

# melody2 = " ".join(melodieliste)

# Wir hätten gerne eine Melodie-Klasse, die folgendes kann:
unsere_melodie = Melodie([57, 59, 60, 62, 64, 60, 64, 64, 63, 59, 63, 63, 62, 58, 62, 62, 57, 59, 60, 62, 64, 60, 64, 69, 67, 64, 60, 64, 67, 67])
melody3 = unsere_melodie.als_string()
# Darum kümmern wir uns dann in der Hausaufgabe

s = Sine(melody3, duration=0.5e3)
s.play()
```
