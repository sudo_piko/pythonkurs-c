# Pikos Python Kurs am 02.02.2023

## Pads

## Protokoll

Pythonkurs am 02.02.2023\
Protokoll von Annemarie

1.  Hinweise am Anfang:

    -   Am 11.02 gibt es um 17 Uhr ein Haecksenfrühstück in Ada:
        <https://meeten.statt-drosseln.de/b/cri-uye-ogv-vfh> - eine gute
        Gelegenheit sich kennenzulernen!

    -   Die Zusatzaufgaben sind weiterführende Knobelaufgaben, muss
        nicht sitzen

    -   Bitte traut Euch, im Kurs immer Fragen zu stellen. Wir sind ein
        Safe-Space, wahrscheinlich haben mehrere Leute die gleichen
        Fragen.

2.  Besprechung der Hausaufgaben

    -   Zu Aufgabe 1 (elif): Elif greift nur, wenn if und else nicht
        gegriffen haben. Elif guckt ob vorher schon irgendetwas
        ausgeführt wurde. Es wird das ausgeführt, was als erstes TRUE
        ist.

    -   Zu Aufgaben 2 (Sternenhimmel): Schaut Euch gerne noch das Video
        dazu an. Siehe auch \[08Loesungen_Sternenhimmel.py\]

    -   ![](media/image1.png){width="3.6319444444444446in"
        height="2.2152777777777777in"}Zu Aufgabe 3 (Dreiecke): siehe
        \[08_Loesungen_DreieickeVerteilen.py\]. Die Koordinaten
        (Koordinate = randint(-300,300)) müssen auf Werte zwischen 0 und
        1 gebracht werden und dann als r,g,b Argumente übergeben werden.
        Damit keine Werte unter 0 übergeben werden, muss dafür erst
        Koordinate+300 gerechnet werden, damit alle Werte positiv sind,
        und dann durch 600 geteilt, damit die Werte zwischen 0-1 sein.
        Mullana malte dies sehr schön ans Whiteboard. Diese Operation
        kommt übrigens häufig vor im Programmiererinnenalltag. Die
        Screensize kann übrigens mit turtle.screensize() herausgefunden
        werden.

    -   Zu Aufgabe 4 (Extra-Fehlerrätsel): eine Python Fehlermeldung ist
        zu sehen. Der Output nimmt nicht die Elemente aus den Listen
        hintereinander pro Datum, sondern liest die Buchstaben der
        strings der Listennamen aus..

3.  Wir bekommen ein Cheatsheet um zu checken, wie man Module, bzw.
    einzelne Funktionen, importiert. \* (Sternchen) bedeutet „alles"
    (was übrigens häufiger so ist). Warum verwendet man nicht immer \*?
    Weil manchmal in zwei Modulen Funktionen vorhanden sein können, die
    den gleichen Namen haben, dann überschreibt der zweite Import die
    Funktion des ersten Imports und alle sind verwirrt.

4.  Bei Funktionsdefinitionen kann man einen Default-Wert setzen, indem
    man in die Funktionsdefinition einen Wert hinter den Variablennamen
    schreibt (def Funktion (Variablennamen=Default-Wert); bspw. def
    stern(size=3)). Dann muss nicht immer ein Argument übergeben werden,
    kann aber.

5.  Heute: Truchet Tiles.

    -   Verändert das Dreieck Programm aus der letzten Woche
        (Hausaufgaben der vorletzten Woche) so, dass man keine Linien
        mehr sieht

    -   Auf diesem Pad ist der vorherige Code mit den 4 Dreiecken
        hinterlegt:
        [https://md.ha.si/z06TRyzySb2PTLOedM-9Mg#](https://md.ha.si/z06TRyzySb2PTLOedM-9Mg)

    -   Truchet Tiles sind nicht nur Dreiecke, sondern sehr vielfältig..
        Siehe kommende Hausaufgabe


## Pikos Datei

![](../Abbildungen/Screenshot_Scaling.png)
```python
from turtle import *
import random

speed(0)

def gefuelltes_dreieck(farbe, seitenlaenge=100):
    color(farbe)
    begin_fill()
    for i in range(3):
        fd(seitenlaenge)
        lt(120)
    end_fill()


# gefuelltes_dreieck((random.random(), (random.random(), 0.5)


for i in range(20):
    x = random.randint(-300, 300)
    y = random.randint(-300, 300)
    pu()
    goto(x, y)
    pd()
    gefuelltes_dreieck((random.random(), (x+300)/600, (y+300)/600), 80)
                        (    0.458     ,     0      ,       0.5  )
                        
    300,300     re ob						1             1
    300,-300    re un						1			0
    300,0	    re mi						1            0.5

```