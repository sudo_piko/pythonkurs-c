# Pikos Python Kurs am 25.05.2023

## Piko erklärt Witze
https://media.hachyderm.io/media_attachments/files/110/413/736/122/725/371/original/fc0aa3539822666e.png
(Quelle: https://hachyderm.io/@inthehands/110413744284749050)

## Pikos Datei
### Hausaufgaben
```python
import playsound
import time

SOUNDFILE = "/home/piko/Schreibtisch/Testordner/mixkit-arcade-retro-game-over-213.wav"

wartezeit_in_minuten = int(input("Wie viele Minuten soll der Tee ziehen?"))
wartezeit_in_sekunden = wartezeit_in_minuten * 60

# print(wartezeit_in_sekunden)
time.sleep(wartezeit_in_sekunden)
playsound.playsound(SOUNDFILE)
```
```python
# wenn in einer Zeile alle "x" sind
# (wenn in einer der Unterlisten alle Einträge "X" sind)
# wenn in einer Spalte alle "x" sind
# wenn in einer Diagonale alle "x" sind

# Hier hat niemand gewonnen
spielfeld_nicht = [[" ", " ", " "],
         [" ", "X", "X"],
         ["O", " ", " "]]

# Hier hat X gewonnen in der zweiten Zeile
spielfeld_zeile = [[" ", " ", " "],
         ["X", "X", "X"],
         ["O", " ", " "]]

def gewonnen_zeile(spielfeld, spiely="X"):
    for zeile in spielfeld:
        if zeile[0] == zeile[1] == zeile[2] == spiely:
            return True
    return False

 # Hier hat O gewonnen in der dritten Spalte
spielfeld_spalte = [["L", "F", "O"],
                    ["E", "X", "O"],
                    ["U", " ", "O"]]

# 
# print(spielfeld_spalte[0][0])
# print(spielfeld_spalte[1][0])
# print(spielfeld_spalte[2][0])
# 
# 
# print(spielfeld_spalte[0][1])
# print(spielfeld_spalte[1][1])
# print(spielfeld_spalte[2][1])


def gewonnen_spalte(spielfeld, spiely="X"):
    for i in range(3):
        if spielfeld[0][i] == spielfeld[1][i] == spielfeld[2][i] == spiely:
            return True
    return False
#     
# print(gewonnen_spalte(spielfeld_spalte, spiely="X"))
# 
# print(gewonnen_spalte(spielfeld_spalte, spiely="O"))

# Hier hat X gewonnen in der ersten Diagonale
spielfeld_diagonale = [["X", " ", "O"],
                       [" ", "X", "O"],
                       ["O", " ", "X"]]

def gewonnen_diagonale(spielfeld, spiely="X"):
    if spielfeld[0][0] == spielfeld[1][1] == spielfeld[2][2] == spiely:
        return True
    if spielfeld[2][0] == spielfeld[1][1] == spielfeld[0][2] == spiely:
        return True
    return False

print(gewonnen_diagonale(spielfeld_diagonale))
print(gewonnen_diagonale(spielfeld_diagonale, spiely="O"))
```

### Pygame
Quelle des Code: https://realpython.com/pygame-a-primer/
Meine Veränderungen:
```python
...  # Some Code
x = 0
growth_rate = 1
size = 100
while ...  # More Code
    screen.fill((0, 0, 0))
    pygame.time.wait(10)
    
    pygame.draw.circle(screen, (0, int(255-x/2), int(x/2)), (x, 250), size)
    x = x + growth_rate
    if x > 500:
        growth_rate = -1
    if x < 0:
        growth_rate = 1
        
...  # More Code
```