# Pikos Python Kurs am 20.07.2023
## Programmierparadigmen

### Imperativer Programmierstil
```python
from turtle import *
fd(20)
dot()
rt(90)
fd(20)
dot()
rt(60)
```
### Funktionaler Programmierstil
```python
def dings(a):
    if a > 10:
        return a * 2
    else:
        return dongs(a) * 28

def lalala(r):
    return r + 1

neu = dings(2)
lalala("hui"*neu)
```

### Objekt-orientierter Programmierstil
zB unsere Melodieklasse


## Pikos Datei
... findet sich in `Projekte/Sound/Melodieklasse. py`

Der unordentliche Teil:
```python
tonobjekt = Ton(60, 2)
tonobjekt2 = Ton(62, 1)
liste_von_strings = [tonobjekt.string_mit_oktave(), tonobjekt2.string_mit_oktave()]
# ["C4", "D4"]
kurze_melodie = " ".join(liste_von_strings)
# "C4 D4"
print(kurze_melodie)
```