# Pikos Python Kurs am 04.05.2023



## Pikos Code
Siehe Datei Diverses/Pikoprobiert/Farbratespiel.py



## Protokoll aus denn geteilten Notizen
Hausaufgaben
* keine Besprechung, da viele Enthaltungen und keine "Neins"

Voraussetzung für die Aufgaben: Single Player Farbrate-Spiel
* 2 Gruppen: kleinere Aufgaben, für die, die noch kein funktionierendes Spiel haben; andere Gruppe: Wie kann man aus dem Spiel generative Art machen? (30min)
** Gruppe Spiel:
*** erster Schritt: Zufallsfarbe definieren
// Sorry, war hier kurz lost...
*** Code:
import random
from turtle import *

def zufallsfarbe():
r = random.random()
g = random.random()
b = random.random()
return r, g, b

farbe = zufallsfarbe()

pensize(200)
color(farbe)
dot()

*** zweiter Schritt: Eingabeaufforderung schreiben
*** Code:
# Farbeingabe
# mit float werden r, g, b Inputs nicht in Anführungszeichen geschrieben

print("Was war das für eine Farbe?")

r = float(input("Rate den Rotwert: "))
g = float(input("Rate den Grünwert: "))
b = float(input("Rate den Blauwert: "))

print(f"Du hast {r, g, b} geraten. Der richtige Wert ist: ", farbe)

*** dritter Schritt: Abweichungen geratener Wert / wirklicher Wert ausgeben lassen
**** zunächst muss die Farbe "entpackt" werden: r_richtig, g_richtig, b_richtig = farbe
*** Code:
# farbe "entpacken"
r_richtig, g_richtig, b_richtig = farbe

print(f"Du hast {r, g, b} geraten. Der richtige Wert ist: ", farbe)

# Abweichungen ausgeben geratener Wert / tatsächlicher Wert

print("Dein Fehler: ")
print("Rot: ", r_richtig -r)
print("Grün: ", g_richtig - g)
print("Blau: ", b_richtig - b)

*** vierter Schritt: einfache Umsetzung generative Art
fehler_r, fehler_g, fehler_b = round(r_richtig-r, 2), round(g_richtig - g, 2), round(b_richtig - b, 2)
kleinste_zahl = min(fehler_r, fehler_g, fehler_b)

neue_farbe = r_richtig - kleinste_zahl, g_richtig - kleinste_zahl, b_richtig - kleinste_zahl

print(f"Du hast {r, g, b} geraten. Der richtige Wert ist: ", farbe)

# Abweichungen ausgeben geratener Wert / tatsächlicher Wert

print("Dein Fehler: ")
print("Rot: ", round(r_richtig - r, 2))
print("Grün: ", round(g_richtig - g, 2))
print("Blau: ", round(b_richtig - b, 2))

color(neue_farbe)
dot()

Alternative IDE's / Texteditor
* einfacher Texteditor (z.B. leafpad):
** Kann nur Zeichen, keine Formatierungen, erkennt aber auch Phythoncode nicht (Einrückungen etc. müssen selbst vorgenommen werden)
** kann auch keinen Code ausführen --> Datei speichern als .py-Datei
** Datei kann in der Kommandozeile / Terminal / Shell vom Rechner ausgeführt werden
*** Befehl für Linux: python DATEIORT/DATEINAME.py --> gibt Farbtupel aus
** weniger komfortabel als Thonny, da auf Syntax selbst geachtet werden muss
Tipps:
*** exitonclick() schließt das Turtle-Fenster bei einem Klick -- notwendig, da das Fenster nicht automatisch beendet wird
*** Pfeil nach oben/unten ruft letzte Befehle in der Kommandozeile auf bzw. "blättert" durch diese durch

* VIM / NeoVIM (kann Farben und Syntaxhighlighting)
** Datei wird in der Kommandozeile bearbeitet und geöffnet
*** vim DATEIORT/DATEINAME.py
** zwei Kommandozeilen: eins mit VIM, eins mit Kommandozeile / Ausgabe
** VIM kann gut ohne Maus verwendet werden, z.B. mit Befehlen: Springe ans Ende der Zeile, lösche ein bestimmtes Wort, ...
** aus VIM rauskommen: einmal esc, damit man in den Schreibmodus kommt und dann :q

* PyCharm
** kann mit GitLab synchronisiert werden
** hat Suchfunktion für Settings
** kann auch andere Sprachen, ist aber eigentlich für Pyhton gedacht
** nimmt einem ganz viel ab, z.B. erstellt für jede geöffnete Klammer automatisch eine geschlossene; markiert Klammern
** hat Copilot (bezahlversion): ChatGPT hilft beim Programmieren
*** unterscheidet Copilot zwischen Kommentar und Frage?
// Vielleicht kann eins hier die Antwort noch ergänzen? Habe das gerade nicht mitbekommen
*** Trainingsmaterial ist öffentlich vorhandener Code von GitHub: Datenbasis ist sehr unterschiedliche und ist auch oft sehr murksiger (Hobby-) Code sein
*** sollen wir nicht nutzen, weil wir da nicht lernen, richtig zu coden --> Nutzung dringend abzuraten

* VSCode
** für mehr Programmiersprachen (nicht nur Python) geeignet


