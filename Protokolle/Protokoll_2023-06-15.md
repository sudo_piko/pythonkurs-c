# Pikos Python Kurs am 15.06.2023


## Pikos Datei

```python
from turtle import *
from math import dist
import random

class Punkt:
    def __init__(self, x, y, farbe):
        self.x = x
        self.y = y
        self.farbe = farbe
        
    def verschieben(self, x=1, y=0):
        self.x = self.x + x
        self.y = self.y + y
        
    def aufmalen(self):
        pu()
        goto(self.x, self.y)
        color(self.farbe)
        dot(20)
        
    def distanz(self, anderer_punkt):
#         print(anderer_punkt.x, anderer_punkt.y)
        d = dist((self.x, self.y), (anderer_punkt.x, anderer_punkt.y))
        return d
        

liste_von_pnukten = []
for i in range(200):
    liste_von_pnukten.append(Punkt(i, random.randint(0, 100), "red"))

p2 = Punkt(55, 2, "blue")
for punkt in liste_von_pnukten:
#     print(punkt.y*punkt.farbe)
    print(punkt.distanz(p2))
```


distanz-Methode ausprobieren:
```python
p1 = Punkt(5, 2, "red")
p2 = Punkt(55, 2, "blue")

entfernung = p1.distanz(p2)

print(entfernung)
```

Fahrradklasse

```python
RAEDERZAHL = 3  # Beispiel für eine Konstante

class Fahrrad:
    def __init__(self, gaenge, reifendurchmesser, reifendruck=3.5, ebike=False, raederzahl=2, reifen_dicht=True):
        self.gaenge = gaenge
        self.reifendurchmesser = reifendurchmesser
        self.reifendruck = reifendruck
        self.ebike = ebike
        self.raederzahl = raederzahl
        self.reifen_dicht = reifen_dicht
        
    def aufpumpen(self, menge):
        if self.reifen_dicht:
            self.reifendruck = self.reifendruck + menge
            if self.reifendruck > 5:
                self.reifendruck = 0
                self.reifen_dicht = False
            
        
pikos_fahrrad = Fahrrad(7, 28, 3.8, raederzahl=RAEDERZAHL)
# "... ist eine Instanz der Klasse Fahrrad"
        
print(pikos_fahrrad.reifendruck)

pikos_fahrrad.aufpumpen(0.5)
print(pikos_fahrrad.reifendruck)

pikos_fahrrad.aufpumpen(0.5)
print(pikos_fahrrad.reifendruck)
pikos_fahrrad.aufpumpen(0.5)
print(pikos_fahrrad.reifendruck)
pikos_fahrrad.aufpumpen(0.5)
print(pikos_fahrrad.reifendruck)
pikos_fahrrad.aufpumpen(0.5)
print(pikos_fahrrad.reifendruck)
pikos_fahrrad.aufpumpen(0.5)
print(pikos_fahrrad.reifendruck)
```

Eine Funktion, die die Distanz zwischen zwei Punkten berechnet: (statt math.dist())

```python
def dist(punkt1, punkt2):
    """gib die Distanz zwischen zwei Punkten zurück"""
    # Pythagoras: a² + b² = c² => c = Wurzel_aus(a² + b²)
    # "wurzel aus" ist genau das gleiche wie "hoch 0.5" (weil es ja das Gegenteil zu "hoch 2" ist!)
    entfernung = ((punkt1[0] - punkt2[0])**2 + (punkt1[1] - punkt2[1])**2)**0.5
    #              a              ²          +            b             ²         hoch 0.5
    return entfernung
```