# Pikos Python Kurs am 06.01.2023

Diesmal wieder mit Videos.

- Hausaufgabe 1: Tic Tac Toe. https://diode.zone/w/3yED8E34pZnFQrWMoz58Mo
- Hausaufgabe 2: pygame. https://diode.zone/w/7APXLWz3nTFRobxG9uQMLK
- Stoff: pygame. https://diode.zone/w/nA8ugmUz5GXqDDxHZpp1R1


## Links
pygame Bilder auf Stack Overflow: https://stackoverflow.com/questions/8873219/how-to-draw-images-and-sprites-in-pygame

Rect-Objekte auf der pygame-Doku: https://www.pygame.org/docs/ref/rect.html

Duck-Typing: https://de.wikipedia.org/wiki/Duck-Typing

## Pikos Datei
Die Datei findet Ihr unter Projekte/pygame/03_pygame_Bild.py