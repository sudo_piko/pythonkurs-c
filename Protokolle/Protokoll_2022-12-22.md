# Pikos Python Kurs am 22.12.2022

## Pads
Spiralen, Lösungen für Aufgabe 2: https://md.ha.si/GOzobU43QiOy9POsA8TcdQ?edit
Schneeflocken: https://md.ha.si/5tAECmMITR-TO_x4Mm70vw#

## Protokoll
Das Turtle Malbuch: https://gitlab.com/naja42/python-malbuch


Was macht circle und die Argumente in der Klammer?

circle(radius, Zeichungsbereich in Grad, Ecken)


setheading(0) - Die Turtle schaut dann in die Ursprungsrichtung

setheading(90) – setzt die turtle auf 90 Grad


False und True:

Der int von True ist 1

Der int von False ist 0


### Module:


import turtle – geht auch, aber dann muss man immer „turtle.“ (den namen
des moduls) vorne dransetzen

Das wäre professioneller, weil mehrere Module die gleiche anweisung
verwenden könnten (z.B. forward) und dann gibts probleme, wenn man die
alle mit * importiert.


from turtle import * - erspart schreibarbeit und ist sinnvoll, wenn man
sonst nix anderes importiert.

- schüttet die ganze legokiste mit allen turtle-steinchen aus


From turtle import forward – importiert nur „forward“

- schüttet nur den baustein „forward“ aus


random bzw. randint

randint(1,8) gibt ne zufallszahl zwischen 1 und 7 aus

random.choise(….) - gibt ein objekt aus der klammer aus

random.random() - gibt eine zufällige zahl zw. 0 und 1 aus


import time

time.sleep(1) – macht etwas jede Sekunde


Zwei == sind eine Frage, ein = ist eine Aufforderung

== vergleicht etwas und führt es aus, wenn es wahr ist

= setzt z.B. eine Variable auf einen bestimmten Wert


## Pikos Datei
Musterlösung für 1.2
```python
from turtle import *

farben = ["red", "green", "blue", "red", "green", "blue"]
pensize(20)
radius = 0


for farbe in farben:
    penup()
    fd(radius)
    pendown()
    lt(90)
    
    
    color(farbe)
    circle(radius)
    rt(90)
    penup()
    fd(-radius)
    pendown()
    
    radius = radius + 20
```

Musterlösung für 1.3
```python
from turtle import *

farben = ["red", "green", "blue", "red", "green", "blue"]
pensize(20)
speed(0)

radius = 20


for farbe in farben:
    penup()
    fd(radius)
    pendown()
    lt(90)
    
    
    color(farbe)
    circle(radius, 180)
    rt(90)
    penup()
    fd(-radius)
    pendown()
    rt(180)
    
    radius = radius + 20
```
Musterlösung für Aufgabe 3
```python
from turtle import *

penup()

entfernung = 10
breite = 10
laenge = 6

for i in range(laenge):
    for j in range(breite):
        dot()
        fd(entfernung)
        
    
    fd(-1 * breite * entfernung)
    rt(90)
    fd(entfernung)
    lt(90)
```
Nachtrag: Lösung für Aufgabe 3 mit `goto()`
```python
from turtle import *

anzahl_spalten = 6  # wie viele Punkte nebeneinander
anzahl_zeilen = 4  # wie viele Punkte übereinander

penup()

for i in range(anzahl_zeilen):
    for j in range(anzahl_spalten):
        goto(j*10, i*10)
        # goto bekommt Koordinaten, also "x, y"
        # j ist "nebeneinander", also die x-Achse, die kommt zuerst.
        dot()
        
# Merkhilfe für x- und y-Achse:
# Wir haben zuerst Kutschen gebaut, und dann erst Helikopter.
# Kutschen = horizontale Bewegung = x-Achse, weil im Alphabet früher.
# Helikopter = Bewegung nach oben = y-Achse, weil später im Alphabet.
```


Aufgabe 4.1
```python
print(type(1))
print(type(1.0))
print(type("Stadterheiterung"))
print(type(str(1.0)))

print(str(False))

print(1.0 * 3)
print("1.0"* 3)
print(str(1.0) * 3)

print(int(str(int(str(int(str(1)))))))
```
Ausgangsprogramm für die Schneeflocke
```python
from turtle import *

for i in range(6):
    fd(100)
    lt(90)
    fd(10)
    lt(90)
    fd(100)
    rt(120)
```
Lösung für die Schneeflocke
```python
from turtle import *

pensize(3)
speed(0)

for i in range(6):
    # Weg vom Mittelpunkt zu den Ärmchen
    fd(60)  
    rt(60)
    fd(10)
    for i in range(3):  # Drei Ärmchen
        fd(50)
        lt(90)
        fd(20)
        rt(90)
        fd(-50)
        lt(60)
    lt(120)
    fd(10)
    # Weg zurück
    rt(60)
    fd(60)
    # Zacken im Zentrum
    rt(120)
    fd(20)
    lt(120)
    fd(20)
    rt(120)
```

Drei Arten von Importen
```python
# Lego-Kiste ins Regal
import turtle
turtle.forward(100)
turtle.right(90)

# Ein Lego-Steinchen überreichen
from turtle import forward
forward(100)

# Lego-Kiste ausschütten
from turtle import *
forward(100)
right(90)
```

```python
from random import randint

print(randint(1, 8))
```

```python
import random
import time

# Zwei == sind eine Frage. Ein = ist eine Aufforderung
for i in range(10):
    zahl = random.randint(1,6)
    if zahl == 6:  # Wenn
        print("Gewonnen!")
    else:  # Andernfalls
        print(zahl)
    time.sleep(1)
# print(random.choice(("erdbeer", "himbeer", "schokolade")))
# print(random.random())

# PyPI
# Python Package Index
```

```python
from random import randint, choice
from time import sleep

# Zwei == sind eine Frage. Ein = ist eine Aufforderung
for i in range(10):
    zahl = randint(1,6)
    if zahl == 6:  # Wenn
        print("Gewonnen!")
    else:  # Andernfalls
        print(zahl)
    sleep(1)
```
