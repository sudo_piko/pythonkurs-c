# Pikos Python Kurs am 30.03.2023

Erstmal muss ich mich entschuldigen, dass ich so kurzfristig bescheidgegeben habe, dass es keinen Kurs, sondern ein 
Video gab. Ich hoffe, da hat sich keine*r extra den Donnerstagabend freigehalten!

Das Treffen am 06.04. findet wieder ganz normal statt.

# Hausaufgabenbesprechung
Hier im Gitlab im Ordner "Diverses/Variations" findet Ihr die Lösungen für die Hausaufgaben.
Hier ein Video, wie ich den Code geschrieben habe; ich habe es im Zug aufgenommen, deshalb gibt es kein Audio:  
https://diode.zone/w/wke77YDPrjKLfuFazedjzt

# Video
Das Video zu den Inhalten dieser Stunde findet Ihr hier:  
https://diode.zone/w/v9KA8GjV6XydbzZPxiwHDh

Es sind zwei Teile; das zweite Video ist in der Videobeschreibung des ersten Videos verlinkt.

Den Code findet Ihr im Ordner "Diverses/Hangman".
