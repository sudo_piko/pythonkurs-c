import pygame

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

pygame.init()
screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
print(type(screen))
run_game = True


circle_size = 10
growth_rate = 1
circle_x = 200
circle_y = 200
circle_x_movement = 3
circle_y_movement = 1


circle_r = 0
circle_g = 150
circle_b = 0

bg_r, bg_g, bg_b = 127, 0, 0

while run_game:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run_game = False
            
    screen.fill((bg_r, bg_g, bg_b))
    pygame.time.wait(10)
    
    pygame.draw.circle(screen, (circle_r, circle_g, circle_b), (circle_x, circle_y), circle_size)
    
    
    
    if circle_size > 200:
        growth_rate = -1
    elif circle_size < 1:
        growth_rate = 1
    circle_size = circle_size + growth_rate
    circle_g = circle_size/2 + 50
    circle_r = 255 - circle_g
    bg_b = 255 - circle_g
    if circle_x > SCREEN_WIDTH:
        circle_x_movement = -3
    elif circle_x < 0:
        circle_x_movement = 3
    circle_x = circle_x + circle_x_movement
#     circle_y = circle_y + circle_x_movement/3
#     if circle_y > SCREEN_HEIGHT:
#         circle_y_movement = -1
#     elif circle_y < 0:
#         circle_y_movement = 1
#     circle_y = circle_y + circle_y_movement
    
    pygame.display.flip()

pygame.quit()
