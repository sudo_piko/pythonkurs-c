import pygame
from pygame.locals import K_UP, K_DOWN, K_LEFT, K_RIGHT, K_ESCAPE, KEYDOWN, QUIT
# More keys at:
# https://www.pygame.org/docs/ref/key.html#module-pygame.key

pygame.init()
screen = pygame.display.set_mode([600, 600])

run_game = True
x = 500
y = 300
while run_game:
    for event in pygame.event.get():
        if event.type == QUIT:
            run_game = False
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                run_game = False
            elif event.key == K_UP:
                y = y - 10
            elif event.key == K_DOWN:
                y = y + 10
            elif event.key == K_LEFT:
                x = x - 10
            elif event.key == K_RIGHT:
                x = x + 10
            
    screen.fill((0, 0, 0))
    pygame.draw.circle(screen, (0, 0, 255), (x, y), 50)
    
    pygame.display.flip()
    # pygame.time.wait(10)

pygame.quit()
