import pygame


pygame.init()
screen = pygame.display.set_mode([600, 600])

run_game = True
while run_game:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run_game = False
            
    screen.fill((0, 0, 0))
#     pygame.time.wait(10)
    
    pygame.draw.circle(screen, (0, 0, 255), (100, 100), 50)        
    pygame.display.flip()

pygame.quit()
