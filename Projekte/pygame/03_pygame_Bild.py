# https://www.pygame.org/docs/ref/rect.html

import pygame


pygame.init()

screen = pygame.display.set_mode([600, 600])

myimage = pygame.image.load("smile.png")
imagerect = myimage.get_rect()
print(type(imagerect))
# schmales_rect = pygame.Rect(200, 300, 10, 200)

run_game = True
while run_game:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run_game = False
            
    screen.fill((0, 0, 0))
#     pygame.time.wait(10)
    screen.blit(myimage, imagerect)
    # move imagerect 1 pixel to the left.
    imagerect = imagerect.inflate(1, 1)
    
    pygame.draw.circle(screen, (0, 0, 255), (500, 500), 50)        
    pygame.display.flip()

pygame.quit()
