

spielfeld = [[" ", " ", " "],
         [" ", "X", "X"],
         ["O", " ", " "]]

def aktualisiere_spielfeld(spielfeld, x, y, zeichen):
    spielfeld[y][x] = zeichen

# Wo ist das `return spielfeld`? Darüber reden wir in der Stunde.

# Alternative:
def aktualisiere_spielfeld(spielfeld, koordinaten, zeichen):
    x, y = koordinaten
    spielfeld[y][x] = zeichen

aktualisiere_spielfeld(spielfeld, (0, 0), "X")
print(spielfeld)