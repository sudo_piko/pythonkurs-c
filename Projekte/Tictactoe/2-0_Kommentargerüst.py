# Das hier ist ein Beispiel für den allerersten Entwurf des Spiels. Hier fehlen wahrscheinlich auch noch einige
# Funktionen, die erst als notwendig auffallen, wenn wir schon ein bisschen weiter sind.

# SETUP


# FUNKTIONEN
# Spielfeld anzeigen-Funktion

# Spielzug abfragen-Funktion (könnte auch die Spielzugausführen-Funktion mitaufrufen)

# Spielzug ausführen-Funktion

# Siegbedingung prüfen-Funktion


# PROGRAMM

# Spielfeld anzeigen
# Spielzug abfragen
# Spielzug ausführen (Spielfeld ändern)
# Siegbedingung prüfen
# nächster Zug