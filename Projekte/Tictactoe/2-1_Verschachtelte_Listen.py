spielfeld = [[0, 1, 2],
         [3, 4, 5],
         [6, 7, 8]]

# Die 4 ausgeben lassen:
print(spielfeld[1][1])

# Die 7 ausgeben lassen:
print(spielfeld[2][1])

# Die 8 ausgeben lassen:
print(spielfeld[2][2])

# Aus der 4 eine "vier" machen:
spielfeld[1][1] = "vier"
print(spielfeld[1][1])