
def eingabe(spiely):
    """Frage nach Koordinaten und gib sie als Tupel aus."""
    print("Spieler*in", spiely, "ist am Zug.")
    # X-Koordinate abfragen:
    x = int(input("x-Koordinate: "))
    # Y-Koordinate abfragen:
    y = int(input("y-Koordinate: "))
    return x, y

koordinaten = eingabe("X")


print(koordinaten)

# Das folgende ist nur Spielerei:
# Wir könnten noch überprüfen, ob die Eingabe gültig ist; also ob das wirklich Zahlen sind, und ob die Zahlen
# auch auf das Spielfeld passen – also bei einem 3*3-Spielfeld zwischen 0 und 2 sind:

def eingabe_mit_test(spiely):
    print("Spieler*in", spiely, "ist am Zug.")
    while True:  # einfach so lange, bis es eine gültige Eingabe gibt
        x = input("x-Koordinate: ")
        y = input("y-Koordinate: ")
        if x in ("0", "1", "2") and y in ("0", "1", "2"):  # Dashier ist dann eine gültige Eingabe
            return int(x), int(y)
        else:
            print("Ungültige Eingabe. Bitte nochmal.")

# Die Zeile "if x in ("0", "1", "2") and y in ("0", "1", "2"):" ist etwas kompliziert. Auseinandergebaut sieht sie so aus:
# if        x in ("0", "1", "2")              and                y in ("0", "1", "2")
# Sagen wir, x ist "1" und y ist "5".
# - "1" kommt in ("0", "1", "2") vor, also wird aus `x in ("0", "1", "2")` => `True`
# - "5" kommt in ("0", "1", "2") nicht vor, also wird aus `y in ("0", "1", "2")` => `False`
# Das wird dann ausgewertet zu zB:
# if        x in ("0", "1", "2")              and                y in ("0", "1", "2")
# if               True                       and                       False
# eine and-Verknüpfung wird immer nur dann True, wenn beide Seiten True sind, sonst ist sie immer False.
# (Das passt auch zu unserer umgangssprachlichen Verwendung von "und":
# "Pythonkurs findet statt, wenn es Donnerstag ist und Piko gesund ist." => Beides muss zutreffen, sonst findet der Kurs nicht statt.)

k = eingabe_mit_test("0")
print(k)