spielfeld = [[" ", " ", " "],
         [" ", "X", "X"],
         ["O", " ", " "]]

# 4*4 Felder, because we're crazy
# spielfeld = [[" ", " ", " ", " "],
#          [" ", "X", "X", "X"],
#          ["O", " ", " ", " "],
#          [" ", "X", "X", "X"]]

# Ich taste mich da jetzt stückchenweise ran; Eure Lösung wird anders aussehen. Wichtig ist, dass sie das Spielfeld leserlich ausgibt.
# Wenn Ihr keine eigene funktionierende Lösung habt, dann schaut Euch das hier an, programmiert es nach, legt es dann beiseite und versucht es nochmal selbst.

# 1. einfach ganz ausgeben
def ausgabe():
    print(spielfeld)


# 2. Zeilenweise ausgeben
def ausgabe():
    for zeile in spielfeld:
        print(zeile)


# 3. Klammern und Kommata weglassen
def ausgabe():
    for zeile in spielfeld:
        for zelle in zeile:
            print(zelle, end=" ")  # end=" " sorgt dafür, dass nach jedem print() kein Zeilenumbruch kommt.
            # Ersetzt end=" " durch end="." und schaut, was passiert!
        print() # Hier kommt der Zeilenumbruch


# 4. Zeilen- und Spaltennummern hinzufügen
def ausgabe():
    print("  0 1 2")  # Allererste Zeile: Spaltennummern.
    for i in range(len(spielfeld)):  # i = 0, 1, 2
        print(i, end=" ")  # ganz am Anfang jeder Zeile eine Zeilennummer
        for zelle in spielfeld[i]:
            print(zelle, end=" ")
        print()


# 5. Zeilentrenner hinzufügen
def ausgabe():
    trenner = " -------"
    print("  0 1 2")
    for i in range(len(spielfeld)):  # Wie oben
        print(i, end=" ")
        for zelle in spielfeld[i]:
            print(zelle, end=" ")
        print()
        if i < len(spielfeld)-1:  # Nur wenn wir nicht in der letzten Zeile sind, soll der trenner ausgegeben werden
            print(trenner)

# 6.a Spaltentrenner hinzufügen

def ausgabe():
    trenner = " | "
    zeilentrenner = " " + "-" * 11
    print("  0   1   2")
    for i in range(3):  # i ist die Zeilennummer, also in welcher Zeile wir gerade sind
        print(i, end=" ")

        # für jede Zelle in der Zeile:
        for j in range(3):  # j ist die Spaltennummer, also wie weit rechts wir gerade sind
            # Die jeweilige Zelle ist spielfeld[i][j]; siehe Aufgabe 2-1
            if j < 2:  # Nur wenn wir nicht in der letzten Spalte sind, soll der trenner ausgegeben werden FUSSNOTE
                print(spielfeld[i][j], end=trenner)
            else:  # sonst gibt es keinen Trenner:
                print(spielfeld[i][j], end=" ")
        print()
        if i < 2:  # Nur wenn wir nicht in der letzten Zeile sind, soll der trenner ausgegeben werden
            print(zeilentrenner)


# 6.b Mit variabler Größe
def ausgabe():
    trenner = " | "
    zeilentrenner = " " + "-" * (len(spielfeld[0])*4 - 1)  # 4 Zeichen pro Zelle, minus 1, weil wir ja auch noch ein Leerzeichen brauchen
    for i in range(len(spielfeld[0])):
        print("  " + str(i), end=" ")  # erste Zeile: Spaltennummern
    print()
    for i in range(len(spielfeld)):  # i ist die Zeilennummer, also in welcher Zeile wir gerade sind
        print(i, end=" ")

        # für jede Zelle in der Zeile:
        for j in range(len(spielfeld[i])):  # j ist die Spaltennummer, also wie weit rechts wir gerade sind
            # Die jeweilige Zelle ist spielfeld[i][j]; siehe Aufgabe 2-1
            if j < len(spielfeld[i])-1:  # Nur wenn wir nicht in der letzten Spalte sind, soll der trenner ausgegeben werden FUSSNOTE
                print(spielfeld[i][j], end=trenner)
            else:  # sonst gibt es keinen Trenner:
                print(spielfeld[i][j], end=" ")
        print()
        if i < len(spielfeld)-1:  # Nur wenn wir nicht in der letzten Zeile sind, soll der trenner ausgegeben werden
            print(zeilentrenner)

# FUSSNOTE: "if j < len(spielfeld[i])-1" sieht ziemlich fies aus. Es hilft aber, das mal auseinanderzubauen:
# - spielfeld ist ja diese verschachtelte Liste.
# - spielfeld[i] ist die i-te Zeile von spielfeld, also zB [" ", "X", "X"]
# - len(spielfeld[i]) ist die Länge dieser Zeile, also zB 3.
#     - Da alle Zeilen gleich lang sind, und wir ja wissen, dass wir Tictactoe spielen, hätte ich hier auch wirklich einfach 3 hinschreiben können.
#     - Es gilt aber als guter Stil, alles, was größer als 1 ist, als grundsätzlich variable Größe anzunehmen. Das ist hier nicht so wichtig, aber es ist eine gute Angewohnheit.
#     - Ich tu hier also so, als könnte es auch TicTacToe mit 4x4 Feldern geben, oder mit 5x5, oder mit 100x100. Für die soll mein Programm auch funktionieren.
# - len(spielfeld[i])-1 ist die Länge der Zeile minus 1, also zB 2.
# - also steht da "if j < 2", also "wenn j kleiner als 2 ist".
# - j ist die Spaltennummer, also nacheinander 0, 1, 2.
#     - 0 ist kleiner als 2, das if bekommt also ein True; also wird der Trenner ausgegeben. Bei 1 auch.
#     - 2 ist aber nicht kleiner als 2, also wird die else-Zeile ausgeführt, und es wird kein Trenner ausgegeben.


# Diese Lösung ist eine Maximalvariante. Eure Lösung muss nicht so kompliziert sein. Hauptsache, sie funktioniert.
# Ihr könntet das natürlich auch die Turtle zeichnen lassen...



ausgabe()