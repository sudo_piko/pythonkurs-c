
# Gewinnbedingungen überprüfen
def gewonnen_zeile(spielfeld, spiely="X"):
    for zeile in spielfeld:
        if zeile[0] == zeile[1] == zeile[2] == spiely:
            return True
    return False

def gewonnen_spalte(spielfeld, spiely="X"):
    for i in range(3):
        if spielfeld[0][i] == spielfeld[1][i] == spielfeld[2][i] == spiely:
            return True
    return False

def gewonnen_diagonale(spielfeld, spiely="X"):
    if spielfeld[0][0] == spielfeld[1][1] == spielfeld[2][2] == spiely:
        return True
    if spielfeld[2][0] == spielfeld[1][1] == spielfeld[0][2] == spiely:
        return True
    return False

def gewonnen(spielfeld, spiely="X"):
    if gewonnen_zeile(spielfeld, spiely):
        return True
    if gewonnen_spalte(spielfeld, spiely):
        return True
    if gewonnen_diagonale(spielfeld, spiely):
        return True
    return False