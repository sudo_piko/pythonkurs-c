# Tictactoe, zusammengebaut


spielfeld = [[" ", " ", " "],
         [" ", " ", " "],
         [" ", " ", " "]]

def ausgabe():
    trenner = " | "
    zeilentrenner = " " + "-" * 11
    print("  0   1   2")
    for i in range(3):  # i ist die Zeilennummer, also in welcher Zeile wir gerade sind
        print(i, end=" ")

        # für jede Zelle in der Zeile:
        for j in range(3):  # j ist die Spaltennummer, also wie weit rechts wir gerade sind
            # Die jeweilige Zelle ist spielfeld[i][j]; siehe Aufgabe 2-1
            if j < 2:  # Nur wenn wir nicht in der letzten Spalte sind, soll der trenner ausgegeben werden FUSSNOTE
                print(spielfeld[i][j], end=trenner)
            else:  # sonst gibt es keinen Trenner:
                print(spielfeld[i][j], end=" ")
        print()
        if i < 2:  # Nur wenn wir nicht in der letzten Zeile sind, soll der trenner ausgegeben werden
            print(zeilentrenner)

def eingabe(spiely):
    """Frage nach Koordinaten und gib sie als Tupel aus."""
    print("Spieler*in", spiely, "ist am Zug.")
    # X-Koordinate abfragen:
    x = int(input("x-Koordinate: "))
    # Y-Koordinate abfragen:
    y = int(input("y-Koordinate: "))
    return x, y

def aktualisiere_spielfeld(spielfeld, koordinaten, zeichen):
    x, y = koordinaten
    spielfeld[y][x] = zeichen


# Gewinnbedingungen überprüfen
def gewonnen_zeile(spielfeld, spiely="X"):
    for zeile in spielfeld:
        if zeile[0] == zeile[1] == zeile[2] == spiely:
            return True
    return False

def gewonnen_spalte(spielfeld, spiely="X"):
    for i in range(3):
        if spielfeld[0][i] == spielfeld[1][i] == spielfeld[2][i] == spiely:
            return True
    return False

def gewonnen_diagonale(spielfeld, spiely="X"):
    if spielfeld[0][0] == spielfeld[1][1] == spielfeld[2][2] == spiely:
        return True
    if spielfeld[2][0] == spielfeld[1][1] == spielfeld[0][2] == spiely:
        return True
    return False

def gewonnen(spielfeld, spiely="X"):
    if gewonnen_zeile(spielfeld, spiely):
        return True
    if gewonnen_spalte(spielfeld, spiely):
        return True
    if gewonnen_diagonale(spielfeld, spiely):
        return True
    return False


am_zug = "X"
    

ausgabe()
while True:
    koords = eingabe(am_zug)
    aktualisiere_spielfeld(spielfeld, koords, am_zug)
    ausgabe()
    if gewonnen(spielfeld, am_zug):
        print(am_zug, "hat gewonnen!")
        break
    if am_zug == "X":
        am_zug = "O"
    else:
        am_zug = "X"