from gensound import Sine
from random import choices

chromatic = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
diatonic = ['C', 'D', "E", 'F', 'G', 'A', 'B', 'C']


melody1 = " ".join(choices(diatonic, k=10))
melody1 = melody1 + " C"
melody2 = " ".join(diatonic)
melody3 = "C E A C"
# Sine("C#-25", 1e3) # C sharp minus 25 cents (1/100th of a semitone)
melody4 = "C C-25 C"  # Hören wir das?

# Baut ein "Glissando" nach unten mit der cents-Syntax!
# "C C-5 C-10 C-15 C-20

toeneliste = ["C"]
for i in range(20, 200, 20):
    toeneliste.append("r=0.1")
    toeneliste.append("C-" + str(i) + "=1")
melody5 = " ".join(toeneliste)


beat = 0.5e3 # 120 bpm
fermata = 0.1 # make fermatas in the melody slightly longer
pause = 2.6 # and breathe for a moment before starting the next phrase
melody6 = "C r=0.1 C-25=1 r=0.1 C=1"  # Mit Pausen!
melody7 = "A r A r=0.5 A r A r A"
#f"r D5 D=2 E=2 F#-13={2+fermata} r={pause} F#=1 {2+fermata} r={pause}" 
# 


s = Sine(melody5, duration=0.5e3)
s.play()

