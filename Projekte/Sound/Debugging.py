from gensound import Sine
from random import choices


notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]

class Ton:
    def __init__(self, midiwert, length=1):
        self.midiwert = midiwert
        self.length = length

    def string_mit_oktave(self):
        """Gibt den Ton als String mit Oktave zurück, zB "D4" """
        oktave = self.midiwert // 12 - 1  # C0 ist leider nicht MIDI-Wert 0, sondern MIDI-Wert 12...
        tonhoehe = self.midiwert % 12  # Stellung in der Oktave: C=0, C#=1 etc. Demnach wie in der Liste notes oben!
        tonname = notes[tonhoehe]
        return tonname + str(oktave)

    def string_mit_laenge(self):
        """Gibt den Ton als String mit Länge zurück, zB "C4=2" """
#         print(self.string_mit_oktave() + "=" + str(self.length))
        return self.string_mit_oktave() + "=" + str(self.length)

# diatonic = ['C', 'D', "E", 'F', 'G', 'A', 'B', 'C']
# melody1 = " ".join(choices(diatonic, k=10))  # => "C D E...


ton1 = Ton(62)
ton2 = Ton(60)

melodie_ohne_laenge = " ".join([ton1.string_mit_oktave()])
melodie_mit_laenge = " ".join([ton1.string_mit_laenge(), ton2.string_mit_laenge()])

print(melodie_mit_laenge)

m_ohne = "D4"
m_mit = "D4=1"

print(melodie_mit_laenge == m_mit)

# melody3 = "D4=2"

s = Sine(melodie_mit_laenge, duration=0.5e3)
s.play()

python gensound just one note ValueError

