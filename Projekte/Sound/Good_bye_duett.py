from gensound import Sine, Triangle
from random import choices


notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]



def midi_zu_frequenz(midiwert):
    frequenz = (440 / 32) * ( 2 ** ((midiwert - 9) / 12))
    return frequenz

class Ton:
    def __init__(self, midiwert, length=1):
        self.midiwert = midiwert
        self.length = length

    def string_mit_oktave(self):
        """Gibt den Ton als String mit Oktave zurück, zB "D4" """
        oktave = self.midiwert // 12 - 1  # C0 ist leider nicht MIDI-Wert 0, sondern MIDI-Wert 12...
        tonhoehe = self.midiwert % 12  # Stellung in der Oktave: C=0, C#=1 etc. Demnach wie in der Liste notes oben!
        tonname = notes[tonhoehe]
        return tonname + str(oktave)

    def string_mit_laenge(self):
        """Gibt den Ton als String mit Länge zurück, zB "C4=2" """
        return self.string_mit_oktave() + "=" + str(self.length)
    
    def frequenz(self):
        """Gibt die Frequenz des Tones in Hertz zurück"""
        frequenz = midi_zu_frequenz(self.midiwert)
        return frequenz
    
    


class Melodie:
    def __init__(self, werteliste, laengenliste):
        tonliste = []
#         for wert in werteliste:  # Ohne Längen ginge das hier, aber mit brauchen wir range...
#             tonliste.append(Ton(wert))
        for i in range(len(werteliste)):
            midiwert = werteliste[i]
            laenge = laengenliste[i]
            tonliste.append(Ton(midiwert, laenge))
        self.tonliste = tonliste
        print(self.tonliste)
    
    def als_string(self):
        """Gibt die Melodie in einem Format aus, das Sine aus gensound verwenden kann."""
        stringliste = []
        for ton in self.tonliste:
            stringliste.append(ton.string_mit_laenge())  # Mit Laenge!
        s = " ".join(stringliste)
        return s
    
# class Musikstueck:
#     def __init ...
#         ...
#         
#     def export_fuer_play(self):
#         ...
#         return ...
#     
#     def spiele_stueck(self):
#         self.musik.play()
        

meine_melodie = Melodie([67, 72, 71, 74, 72, 74, 76, 76, 74, 77, 76, 72, 84, 83, 79, 84, 83, 80, 77, 80 ], [2, 4, 1, 1, 7, 1, 2, 4, 0.67, 0.67, 0.66, 3, 1, 0.67, 0.67, 0.66, 0.67, 0.67, 0.66,1])
melodiestring = meine_melodie.als_string()

zweite_melodie = Melodie([48, 52, 50, 47, 48, 48, 52, 55, 52, 48, 52, 47, 48, 50, 55, 48, 49], [2, 2, 2, 2, 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2])
melodiestring2 = zweite_melodie.als_string()

laenge = 0.5e3
s = Sine(melodiestring, duration=laenge)
s2 = Triangle(melodiestring2, duration=laenge)
duett = s + s2
duett.play()


duett = s + s2

# xxx = mein_musikstueck.export_fuer_play()
# xxx.play()
# 
# mein_musiktstueck.spiele_stueck()

# Wir machen die Melodie mit S = sig("...", duration=...)
# Dann in ein objekt namens "chorale": chorale = S* Pan(...) + B * Pan(...)
# dann chorale.play()





