# Glissando sine sweep nach oben

from gensound import Sine

l = [str(float((i/4)**2)) for i in range(16, 300)]
melody = " ".join(l)

print(melody)

s = Sine(melody, duration=0.5e1)
s.play()


#D5 C A F# B G# E# C# F#
