from gensound import Sine
import random


tonvorrat = ['C', 'D', 'E', 'F', 'G', 'A', 'B']

# toene = random.choices(tonvorrat, k=10)


def benachbarte_toene(ton):
    tonindex = tonvorrat.index(ton)  # 2
    nachbarnliste = [tonvorrat[tonindex-1], tonvorrat[tonindex+1]]
#                     [tonvorrat[1]     , tonvorrat [3]]
    
    return nachbarnliste


def tonanhaengen(tonliste):
    letzter = tonliste[-1][0]
    if letzter == "B":
        print("Letztes Element ist ein B! Hänge C an...")
        ton = "C"  
    elif letzter == "F":
        if random.randint(1, 10) > 7:
            ton = random.choice(benachbarte_toene(letzter))
        else:
            ton = random.choice(['D', 'E', 'F', 'G', 'A'])
    elif letzter == "C":
        ton = random.choice(tonvorrat)
        # nur manchmal ausführen, nicht immer!
        if random.randint(1, 6) > 3:
            if ton in ["G", "A", "C"]:
                ton = ton + "'"
        if random.randint(1, 6) > 4:
            if ton in ["D", "E", "F", "C"]:
                ton = ton + ","
    elif letzter == "D":
        if random.randint(1, 10) > 7:
            ton = random.choice(benachbarte_toene(letzter))
        else:
            ton = random.choice(tonvorrat)
        
    elif letzter == "E":
        if random.randint(1, 10) > 7:
            ton = random.choice(benachbarte_toene(letzter))
        else:
            ton = random.choice(tonvorrat)
    elif letzter == "G":
        if random.randint(1, 10) > 7:
            ton = random.choice(benachbarte_toene(letzter))
        else:
            ton = random.choice(tonvorrat)
    elif letzter == "A":
        if random.randint(1, 10) > 7:
            ton = random.choice(benachbarte_toene(letzter))
        else:
            ton = random.choice(tonvorrat)
    tonliste.append(ton)
    
# 
# d, e, f : gehen eigentlich nach oben. können aber hier auch nach unten gehen
# g, a, b: gehen eigentlich nach unten, können aber hier auch nach oben gehen.    


toene = ["C"]
for i in range(10):
    tonanhaengen(toene)

toene.append(random.choice(["G", "H", "D"]))
toene.append("C")

print(toene)
melodie = " ".join(toene)
print(melodie)

s = Sine(melodie, duration=0.5e3)
s.play()