from gensound import Sine
from random import choices


notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"]



def midi_zu_frequenz(midiwert):
    frequenz = (440 / 32) * ( 2 ** ((midiwert - 9) / 12))
    return frequenz

class Ton:
    def __init__(self, midiwert, length=1):
        self.midiwert = midiwert
        self.length = length

    def string_mit_oktave(self):
        """Gibt den Ton als String mit Oktave zurück, zB "D4" """
        oktave = self.midiwert // 12 - 1  # C0 ist leider nicht MIDI-Wert 0, sondern MIDI-Wert 12...
        tonhoehe = self.midiwert % 12  # Stellung in der Oktave: C=0, C#=1 etc. Demnach wie in der Liste notes oben!
        tonname = notes[tonhoehe]
        return tonname + str(oktave)

    def string_mit_laenge(self):
        """Gibt den Ton als String mit Länge zurück, zB "C4=2" """
        return self.string_mit_oktave() + "=" + str(self.length)
    
    def frequenz(self):
        """Gibt die Frequenz des Tones in Hertz zurück"""
        frequenz = midi_zu_frequenz(self.midiwert)
        return frequenz
    
    


class Melodie:
    def __init__(self, werteliste, laengenliste):
        tonliste = []
#         for wert in werteliste:  # Ohne Längen ginge das hier, aber mit brauchen wir range...
#             tonliste.append(Ton(wert))
        for i in range(len(werteliste)):
            midiwert = werteliste[i]
            laenge = laengenliste[i]
            tonliste.append(Ton(midiwert, laenge))
        self.tonliste = tonliste
        print(self.tonliste)
    
    def als_string(self):
        """Gibt die Melodie in einem Format aus, das Sine aus gensound verwenden kann."""
        stringliste = []
        for ton in self.tonliste:
            stringliste.append(ton.string_mit_laenge())  # Mit Laenge!
        s = " ".join(stringliste)
        return s


meine_melodie = Melodie([71, 72, 67, 64, 60], [2, 6, 1, 1, 8])
melodiestring = meine_melodie.als_string()
s = Sine(melodiestring, duration=0.2e3)
s.play()