from gensound import Sine
from random import choices

chromatic = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
diatonic = ['C', 'D', 'E', 'F', 'G', 'A', 'B']

notes = diatonic
melody = " ".join(choices(notes, k=25))
melody2 = " ".join(choices(notes, k=50))


s = Sine(melody, duration=500.) + Sine(melody2, duration=250.)
s.play()


