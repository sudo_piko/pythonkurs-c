
from turtle import *
from math import dist
import random
import time

HOEHE = 600
BREITE = 800
SCHRITTWEITE = 5

class Punkt:
    def __init__(self, x, y, farbe):
        self.x = x
        self.y = y
        self.farbe = farbe
        
    def verschieben(self, x=1, y=0):
        self.x = self.x + x
        self.y = self.y + y
        
    def aufmalen(self):
        pu()
        goto(self.x, self.y)
        color(self.farbe)
        dot(5)
        
    def distanz(self, anderer_punkt):
#         print(anderer_punkt.x, anderer_punkt.y)
        d = dist((self.x, self.y), (anderer_punkt.x, anderer_punkt.y))
        return d
    
    def distanz_zu_koordinaten(self, koordinaten=(0, 0)):
        d = dist((self.x, self.y), koordinaten)
        return d
    

# p1 = Punkt(50, 200, "red")
# p1.aufmalen()

# Punkt(50, 200, "red").aufmalen()


punkteliste = []
for i in range(10):
    neues_punktobjekt = Punkt(
        random.randint(-0.5*BREITE, 0.5*BREITE),
        random.randint(-0.5*HOEHE, 0.5*HOEHE),
        (random.random(), random.random(), random.random())
        )
    punkteliste.append(neues_punktobjekt)

for punkt in punkteliste:
    punkt.aufmalen()
    print(punkt.distanz_zu_koordinaten((0, 0)))



def finde_naechsten_mittelpunkt(pixelkoordinate, mittelpunkteliste):
    kleinste_entfernung = 10000
    # alle Voronoi-Mittelpunkte durchgehen:
    for mittelpunkt in mittelpunkteliste:
        # distanz messen
        entfernung = mittelpunkt.distanz_zu_koordinaten(pixelkoordinate)
        if entfernung < kleinste_entfernung:  # wenn entfernung kleiner als bisheriger Favorit
            kleinste_entfernung = entfernung  # dann ist das die neue kleinste Entfernung
            naechster_punkt = mittelpunkt  # und dann ist auch der derzeitig angeschaute Punkt der nächste Punkt
    return naechster_punkt

print("nah:", finde_naechsten_mittelpunkt((0, 0), punkteliste).farbe)

tracer(False)

anfangszeit = time.time()

for i in range(int(-0.5*HOEHE), int(0.5*HOEHE), SCHRITTWEITE):
    for j in range(int(-0.5*BREITE), int(0.5*BREITE), SCHRITTWEITE):
        # hier kommt der Code rein, der für jeden Pixel ausgeführt werden soll
        goto(j, i)
        
        farbe = finde_naechsten_mittelpunkt((j, i), punkteliste).farbe
        color(farbe)
        dot(SCHRITTWEITE + 2)

endzeit = time.time()
print(endzeit -anfangszeit)
tracer(True)
# 
# p2 = Punkt(55, 2, "blue")
# 
# entfernung = p1.distanz(p2)
# 
# print(entfernung)


