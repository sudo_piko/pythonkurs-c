# zum Dranflanschen an das Farbratespiel. Da kommen am Ende zwei Werte raus:

richtige_farbe = (0.7, 0.2, 0.4)
geratene_farbe = (0.5, 0.2, 0.3)


# die erste Variante ist einfach:
def unterschied_betrag(richtige_farbe, geratene_farbe):
    unterschied = (richtige_farbe[0] - geratene_farbe[0], richtige_farbe[1] - geratene_farbe[1], richtige_farbe[2] - geratene_farbe[2])
    # betragsfunktion: abs()
    return (abs(unterschied[0]), abs(unterschied[1]), abs(unterschied[2]))

# der "ins Positive verschobene" Unterschied ist etwas komplizierter:
def unterschied_positiv_verschoben(richtige_farbe, geratene_farbe):
    unterschied = (richtige_farbe[0] - geratene_farbe[0], richtige_farbe[1] - geratene_farbe[1], richtige_farbe[2] - geratene_farbe[2])

    # wir müssen unterscheiden, ob einer der Werte negativ ist:
    if unterschied[0] < 0 or unterschied[1] < 0 or unterschied[2] < 0:  # wenn einer der Werte negativ ist, dann:
        kleinster_wert = min(unterschied)  # wir finden den kleinsten ("negativsten") Wert
        # und verschieben alle Werte um diesen Wert nach oben:
        ergebnis = (unterschied[0] - kleinster_wert, unterschied[1] - kleinster_wert, unterschied[2] - kleinster_wert)
        # hier könnte es aber sein, dass einer der Werte über eins hinausgeht! Das müssen wir noch abfangen:
        ergebnis = (min(1, ergebnis[0]), min(1, ergebnis[1]), min(1, ergebnis[2]))
        # Da wird jetzt für jeden Wert entweder 1 oder der Wert selbst genommen, je nachdem, was kleiner ist.
        return ergebnis

    else:  # wenn keiner der Werte negativ ist, dann müssen wir nichts ändern:
        return unterschied


# in der Aufgabenstellung gab es ein "ß". Das ist ganz dünnes Eis, das war eine Unaufmerksamkeit meinerseits. Probiert gern aus,
# ob das auf Euren Computern funktioniert. Grundsätzlich solltet Ihr für Code, der mit Text zu tun hat, nur Buchstaben
# verwenden, die im englischen üblich sind, selbst wenn es scheinbar trotzdem funktioniert. Deshalb habe ich das "ß" hier
# durch "ss" ersetzt:
def unterschied_weiss_invertiert(unterschied):
    ergebnis = (1 - unterschied[0], 1 - unterschied[1], 1 - unterschied[2])
    # Das macht aus (0.5, 0.2, 0.1) (also einem dunklen rot) => (0.5, 0.8, 0.9) (also ein helles cyan)
    return ergebnis