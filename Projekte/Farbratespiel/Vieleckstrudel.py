# IMPORTE
from turtle import *

# SETUP
# Hintergrund schwarz
bgcolor("black")
# sorgt dafür, dass die Zeichnung sofort erscheint und nicht aufgebaut wird:
tracer(False)


# FUNKTIONEN
def vieleck_von_mittelpunkt(radius, ecken=4):
    # Aus dem Mittelpunkt raus und nach links drehen
    pu()
    fd(radius)
    lt(90)
    pd()

    # ein Vieleck zeichnen
    circle(radius, 360, ecken)

    # wieder zurück zum Mittelpunkt
    pu()
    lt(-90)
    fd(-radius)
    pd()

def vieleckstrudel(alpha=0, factor=0.9, seite=400, ecken=3, farbe=(1,1,0)):
    color(farbe)
    while seite > 20:
        # Wir sitzen immer im Mittelpunkt unserer Zeichnung, das heißt auch im Mittelpunkt aller Vielecke
        vieleck_von_mittelpunkt(seite, ecken)
        # Wir drehen uns um alpha Grad nach rechts
        rt(alpha)
        # factor ist der Wert, um den die Seitenlänge bei jedem Schritt kleiner wird
        seite = seite * factor


# PROGRAMM
vieleckstrudel()







# Piko wollte einen Glow-Effect, courtesy Mullana:
# for i in range(5):
#     pensize(9-2*i)
#     v = 0.1 + i*0.2
#     vieleckstrudel(ecken=5, farbe=(v, v, 0))
#     goto(0,0)
#     setheading(0)

exitonclick()


