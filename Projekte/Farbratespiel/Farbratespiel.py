from random import random
from turtle import *


setup(600, 480, startx=100, starty=100)
def zufallsfarbe():
	r = random()
	g = random()
	b = random()
	return r, g, b

farbe = zufallsfarbe()

pensize(200)
color(farbe)
dot()

# Die Vermutung der Spielenden abfragen
print("Was war das für eine Farbe?")
r = float(input("Rate den Rotwert: "))
# Auf zwei Stellen runden
r = round(r, 2)

g = float(input("Rate den Grünwert: "))
g = round(g, 2)

b = float(input("Rate den Blauwert: "))
b = round(b, 2)

# Die richtigen Antworten
r_richtig = round(farbe[0], 2)
g_richtig = round(farbe[1], 2)
b_richtig = round(farbe[2], 2)

# # Den Fehler berechnen
# fehler_r, fehler_g, fehler_b = round(r_richtig-r, 2), round(g_richtig-g, 2), round(b_richtig-b, 2)
# kleinste_zahl = min(fehler_r, fehler_g, fehler_b)
#
# # Die neue Farbe
# neue_farbe = (r_richtig-kleinste_zahl, g_richtig-kleinste_zahl, b_richtig-kleinste_zahl)

# Ausgabe
print(f"Du hast {r, g, b} geraten. der richtige Wert ist:", farbe)
print("""Dein Fehler:""")
print("Rot:  ", round(r_richtig-r, 2))
print("Grün: ", round(g_richtig-g, 2))
print("Blau: ", round(b_richtig-b, 2))

# color(neue_farbe)
# dot()


exitonclick()
