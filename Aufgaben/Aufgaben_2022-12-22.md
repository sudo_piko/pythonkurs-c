# Aufgaben am 22.12.2022

# 0 - Lesestoff

## 1 – Strings zusammenbauen
### Strings in `print()`
Was ist der Unterschied zwischen den folgenden Code-Schnipseln? Was ist der Unterschied im Ergebnis?
```python
print('Jeder', 'Mensch', 'hat', 'das', 'Recht', 'sich', 'zu', 'irren.')

print('Jeder' + 'Mensch' + 'hat' + 'das' + 'Recht' + 'sich' + 'zu' + 'irren.')
```

### Strings und integers in `print()`
Was ist der Unterschied zwischen den folgenden Code-Schnipseln? Was ist der Unterschied im Ergebnis?
```python
print("Wir haben", "heute", 25, "Orangen", "gekauft.")

print("Wir haben" + "heute" + 25 + "Orangen" + "gekauft.")
```

## 2 – Matheübungsprogramm


```python
antwort = input("Was ist 12 + 11? ")

if antwort == str(23):
    print("Richtig!")
else:
    print("Falsch!")
```

- Was passiert hier? Schaut mal in die Kommandozeile, da könnt Ihr (wegen `input()`) etwas eingeben!
- Fallen Euch Anwendungsmöglichkeiten ein?
- Packt die Zahlen (hier 12, 11 und 23) in Variablen, sodass sie ganz am Anfang des Programms definiert werden.

<details> 
  <summary>Tipps </summary>
    Die Variablennamen müssen außerhalb der Anführungsstriche sein, sonst stellt sich Python dumm...
</details>

<details> 
  <summary>Tipps </summary>
    Guckt auch, was in Aufgabe 1 los war! Das `str()` in der Zeile mit dem `if` ist wichtig!
</details>

- Lasst die 23 (also das Ergebnis) automatisiert ausrechnen, sodass Ihr immer nur zwei Zahlen verändern müsst, wenn Ihr andere Aufgaben haben wollt.

<details> 
  <summary>Tipps </summary>
    Die dritte Zahl (im Beispiel 23) ist ja immer die Summe der beiden anderen Zahlen. Die lässt sich also ganz einfach zuweisen: `zahl3 = ...`
</details>

- Importiert random und lasst die beiden Zahlen zufällig generieren!


<details> 
  <summary>Tipps </summary>
   Im Protokoll findet sich der Code, den ich in der Stunde vorgestellt habe. Da könnt Ihr ganz viel wiederverwenden!
</details>


## 2 – mehr `random`
### 2.1 Fremder Code
Was macht folgendes Programm? Wie funktioniert begin_fill und end_fill?
```python
from turtle import *

fillcolor("red")

begin_fill()
circle(70)  # probiert es auch mit circle(70, 180) !
end_fill()
```

### 2.2 Quadrate
- Schreibt ein Programm, das zufällig ein schwarzes oder weißes Quadrat malt. Ihr braucht dafür `random.choice(("black", "white"))`
- Kopiert Eure Lösung von Aufgabe 3 von letzter Woche (oder auch die Lösung im Protokoll), und ersetzt die Zeile `dot()` mit dem Programm, das die Quadrate malt. 
  - Achtet dabei darauf, dass Ihr in jeder for-Schleife einen anderen Buchstaben verwendet (also erst i, dann j, dann k...), sonst werden die Quadrate scheinbar zufällig verteilt...
  - Ändert die Längen so, dass die Quadrate einander berühren, sodass ein zufälliges Schachbrettmuster entsteht.


<details> 
  <summary>Tipps </summary>
    Achtet auf die richtigen Einrückungen.
</details>

<details> 
  <summary>Tipps </summary>
    Um Euch besser zu orientieren, könnt Ihr erstmal "red" statt "white" nehmen, dann seht Ihr die Quadrate immer...
</details>
<details> 
  <summary>Tipps </summary>
    Die import-Statements gehören ganz nach oben.
</details>
<details> 
  <summary>Tipps </summary>
    Wenn Ihr die Orientierung verliert, fangt von dem dot-Programm an und ersetzt die Dots erstmal mit Quadraten in einer 
for-Schleife (oder mit `circle(laenge, 360, 4)` :D). Dafür braucht Ihr auch pendown(). Bringt das zum Laufen. Dann gebt 
allen Quadraten eine rote Füllung. Und erst dann lasst die Füllung zufällig bestimmen.
</details>

## 3 – Regenbogenlösungen
Packt, wenn Ihr möchtet, Eure Lösungen und Screenshots für die Regenbögen hier rein:  
https://md.ha.si/AO2e_MfpSSW033tLJ8cGKg#