# Aufgaben am 14.07.2023

## 0 - Debugging-Video
Die Fehlermeldung, die wir in der letzten Stunde hatten, habe ich in diesem Video genauer überprüft:  
https://diode.zone/w/gEUiHj3AP1RMW8jkoYHEUv

## 1 – Melodie-Klasse

Am Ende der letzten Stunde hat Piko sich eine Melodieklasse erträumt, die so funktionieren soll:

```python
...
meine_melodie = Melodie([71, 72, 67, 64, 60])
melodiestring = meine_melodie.als_string()
s = Sine(melodiestring, duration=0.5e3)
s.play()
```

Aus diesem Codeschnipsel können wir rauslesen, dass die Melodieklasse eine Liste von MIDI-Werten übergeben bekommt, und
dass ihre Methode `als_string()` einen String ausgibt, der für die Klasse `Sine` geeignet ist.

:zap: Bevor wir an die Melodieklasse gehen, schaut einmal, wie Ihr die Zahlenliste im Beispiel mit der Ton-Klasse 
und `join()` in einen String für `Sine` umwandeln könnt. (Das ist sehr ähnlich zu den Aufgaben in der letzten Zeit.)

:zap: Jetzt wollen wir die Melodieklasse bauen. 
- Sie soll ersteinmal nur eine Liste an Ton-Objekten als einziges Attribut haben. Diese Liste an Ton-Objekten müsst 
  Ihr innerhalb der `__init__()`-Methode mit einer Schleife aus der Liste an MIDI-Werten erzeugen.
  - Weil Ihr Ton-Objekte braucht, muss natürlich auch die Definition der `Ton`-Klasse in Eurem Programm stehen.
- Sie hat erstmal nur zwei Methoden, nämlich `__init__()` und `als_string()`.
- Für `als_string()` braucht Ihr Code, der sehr ähnlich zu dem ist, was Ihr oben geschrieben habt, um die 
  Zahlenliste im Beispiel abzuspielen.
- Die letzten vier Zeilen Eures Programms sollten genau die vier Zeilen oben im Beispiel sein.

Schnappt Euch eine andere Klasse als Vorbild, z.B. die `Ton`-Klasse, und baut die `Melodie`-Klasse.

### Tipps
<details>
<summary>I'm completely lost</summary>
Im Abbildungs-Ordner findet Ihr ein Cheatsheet zu Klassen. Statt `dings` und `dongs` müssen da andere Dinge hin, und 
Ihr müsst auch in der `__init__()`-Methode eine Schleife einbauen, damit aus allen MIDI-Werten Ton-Objekte werden.
</details>

zu `TypeError: sequence item 0: expected str instance, Ton found`  
Tipps in steigender Ausführlichkeit; also lest erstmal nur den ersten und versucht, das Problem zu lösen. 
<details> 
  <summary>1</summary>
   Versucht bei dieser Fehlermeldung Euch erstmal klar zu werden, was das Problem ist. Im zweiten Schritt müsst Ihr 
Euch dann fragen, warum das Problem auftritt, bzw. was fehlt, was hätte noch gemacht werden müssen. In welcher Zeile 
tritt das Problem auf? Was wird in dieser Zeile versucht?
</details>

<details>
  <summary>2</summary>
Wahrscheinlich zeigt die Fehlermeldung zwei Zeilen an (bei mir `line 39` und `line 34`). Die letzte Zeile, die da 
angezeigt wird (bei mir `line 34`), ist die interessante. Was wird in dieser Zeile versucht? Welche Methode wird 
ausgeführt? Was für Objekte will diese Methode zwischen die Klammern bekommen? Recherchiert das gerne im Internet!
</details>

<details>
  <summary>3</summary>
Es geht um `join()`. Bisher haben wir `join()` zwischen die Klammern immer so Sachen wie 
`["C", "D", "E", "F", "G", "A", "B"]` übergeben. Was sind das für Objekte in dieser Liste? Strings natürlich! Welche 
Objekte versuchen wir gerade in `join()` zu übergeben? 
</details>

<details>
  <summary>4</summary>
Wir versuchen gerade, Ton-Objekte an `join()` zu übergeben... Damit weiß `join()` nichts anzufangen. Wir müssen also 
irgendwie aus diesen Ton-Objekten Strings machen – und wir haben Methoden, die das können.
</details>

<details>
  <summary>5</summary>
Wir müssen also aus jedem dieser Ton-Objekte einen String machen. Aber dafür müssen wir die Liste einmal ganz 
auspacken, denn als ganze können wir das nicht. Wir müssen also eine Schleife über die Liste laufen lassen, und
jedes Objekt in der Liste in einen String umwandeln und in eine neue Liste reintun (und, wie immer bei solchen 
Listen-Befüll-Aktionen, müssen wir *vor* der Schleife eine leere Liste anlegen).
</details>

### Notizen für die Melodie-Klasse
übergeben: Liste von midi-Werten  

Attribute:
- ob swingend oder nicht
- stotternd (oder andere Artikulationen, Lautstärke)
- Töneliste
- Geschwindigkeit

Methoden:
- Töne durcheinanderwürfeln
- "als_string": Melodie so ausgeben, dass sie in Sine reingepackt werden kann

## 2 – Tonklasse: Frequenz

Töne sind ja eigentlich Luftschwingungen. Das heißt, die Luft schwingt so und so oft, wenn wir einen Ton hören – zB 
440 mal pro Sekunde bei einem A4.

:zap: Baut in die Ton-Klasse eine neue Methode ein, die die Frequenz des Tons zurückgibt. Dafür braucht Ihr folgende 
Funktion, die Euch aus einem Midi-Wert den Frequenzwert berechnet:

```python
def midi_zu_frequenz(midiwert):
    frequenz = (440 / 32) * ( 2 ** ((midiwert - 9) / 12))
    return frequenz
```

:zap: Probiert die Methode aus, indem Ihr ein Ton-Objekt erzeugt und die Methode aufruft. Welche Beziehung besteht 
zwischen den Frequenzen zweier Töne, die eine Oktave (also zwölf Midi-Werte) auseinander liegen?
