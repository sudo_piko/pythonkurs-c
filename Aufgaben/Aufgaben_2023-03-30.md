# Aufgaben am 30.03.2023

## 1 – Mehr Wörter für Hangman

Sucht Euch schöne Wörterlisten, zB hier:  
https://namingschemes.com

... oder irgendwo im Netz. Macht schöne Python-Listen daraus und packt sie in `woerter.py`. Spielt damit herum!


## 2 – Hangman verbessern

Der Version, die ich im Video baue, fehlt ja eigentlich noch eine ganze Menge. Hier sind ein paar Ideen:
- Nicht nur die Fehlschläge ausgeben, sondern eine ASCII-Graphik mit dem Galgen. (Siehe unten; ASCII siehe: https://de.
  wikipedia.org/wiki/ASCII-Art)
- Fehlermeldung, wenn eins versehentlich mehr als einen Buchstaben oder eine Zahl oder so eingibt

```
      _______
     |  /    |
     | /     O
     |/     \|/
     |       |
     |      / \
     |
  ___|___
```


## 3 – Farben-Rate-Spiel

Ich möchte demnächst mit Euch ein Farben-Rate-Spiel bauen: Die Spielenden bekommen eine Farbe zu sehen, zB folgende:  
![gruen.png](..%2FAbbildungen%2Fgruen.png)  
und sollen den RGB-Wert (in diesem Fall `(0.6, 0.8, 0)`) dazu erraten.

Vielleicht habt Ihr schon eine Datei namens `turtle_helpers.py` oder so. Das wäre ein guter Ort für die Funktion 
`zufallsfarbe()`. Die Funktion soll eine zufällige Farbe zurückgeben, also zB genau dieses `(0.6, 0.8, 0)`. Baut die 
Funktion und importiert sie in einem Programm, das Euch einen Kreis in einer zufälligen Farbe ausgibt (von da aus 
können wir dann weiterarbeiten!).

Wenn Ihr Lust habt, könnt Ihr auch schon mal weiter überlegen: Was braucht das Spiel sonst noch?