# Aufgaben am 18.05.2023

## 1 – Tic Tac Toe

Wir hatten ja die Gewinnbedingungen in Umgangssprache formuliert:
"X" hat gewonnen,
- wenn in einer Zeile alle "x" sind
	(wenn in einer der Unterlisten alle Einträge "X" sind)
- wenn in einer Spalte alle "x" sind
- wenn in einer Diagonale alle "x" sind

Das muss jetzt noch in Code gegossen werden.

Hier einige Beispiele, wie das Spielfeld aussehen könnte; kopiert die Euch gerne in Eure Datei:
```python
# Hier hat niemand gewonnen
spielfeld = [[" ", " ", " "],
         [" ", "X", "X"],
         ["O", " ", " "]]

# Hier hat X gewonnen in der zweiten Zeile
spielfeld = [[" ", " ", " "],
         ["X", "X", "X"],
         ["O", " ", " "]]

 # Hier hat O gewonnen in der dritten Spalte
spielfeld = [[" ", " ", "O"],
         ["X", "X", "O"],
         ["O", " ", "O"]]

# Hier hat X gewonnen in der ersten Diagonale
spielfeld = [["X", " ", "O"],
         [" ", "X", "O"],
         ["O", " ", "X"]]
```

:zap: Baut für die drei Gewinnbedingungen jeweils eine Funktion, die `True` zurückgibt, wenn die Bedingung erfüllt ist.
Testet sie mit den Beispielen! Bedenkt, dass die Funktion immer auch die spiely-Person übergeben bekommen sollte:
`def gewonnen_zeile(spiely):` und dann beim Aufruf: `ergebnis = gewonnen_zeile("X")`


<details> 
  <summary>Vorgehen </summary>
   Fangt erstmal mit der ersten Funktion an und lasst Euch ausgeben, was die Funktion returnt. Die beiden anderen 
Funktionen sind ähnlich, lassen sich also nach dem Vorbild der ersten bauen.
</details>

<details>
	<summary>Wie finde ich heraus, ob in einer Zeile alle Einträge "X" sind?</summary>
	Die "Zeilen" des Spielfelds sind ja Unterlisten der Liste `spielfeld`. Um die erste Zeile zu bekommen, könnt Ihr 
also `spielfeld[0]` schreiben. Das ist dann zB `[" ", "X", " "]`. Um zu überprüfen, ob alle Einträge "X" sind, könnt 
Ihr so etwas schreiben: `(liste[0] == "X") and (liste[1] == "X") and (liste[2] == "X")`. Das ist zwar etwas 
umständlich, aber es funktioniert. Ihr könnt auch eine Schleife verwenden, die über die Liste läuft und überprüft, 
ob alle Einträge "X" sind.
</details>

<details>
	<summary>Wann returne ich True und wann False?</summary>
	Wenn alle Einträge "X" sind, dann soll die Funktion `True` zurückgeben. Wenn nicht, dann `False`. Das klingt aber
einfacher als es ist. Denn wenn die erste Bedingung (also "alles X in der ersten Zeile") nicht erfüllt ist, dann soll 
die Funktion ja nicht direkt `False` zurückgeben, sondern erstmal überprüfen, was denn in der zweiten Zeile los ist..
. Also: Wenn die erste Zeile gewonnen ist, ist der Fall klar, da darf dann direkt `True` zurückgegeben werden. Wenn 
sie nicht gewonnen ist, dann muss die Funktion weiter überprüfen, ob die zweite Zeile gewonnen ist. Und so weiter. 
Erst wenn wir am Ende der dritten Zeile angekommen sind, und immer noch nicht `True` zurückgegeben haben, dann 
können wir `False` zurückgeben.
</details>

<details>
	<summary>Wie komme ich denn an die Spalten?</summary>
	Darüber hatten wir in der Stunde gesprochen; schaut Euch den Code im Protokoll an und schreibt ihn ab. Verändert 
einzelne Details und schaut Euch an, wie sich das Ergebnis verändert!
</details>
<details>
	<summary>Wie komme ich an die Diagonale?</summary>
	Da gibt es leider keine elegante Lösung; da müsst Ihr Euch die Einträge einzeln raussuchen...
</details>

## 2 – playsound

Für diese Aufgabe braucht Ihr eine Sound-Datei. Nehmt gerne irgendeine mp3; wenn Ihr nichts findet, könnt Ihr Euch
[hier](https://mixkit.co/free-sound-effects/) ein .wav-File herunterladen.

:zap: Speichert die Datei im selben Ordner wie Euer Python-Programm, gebt ihr einen einfachen Namen.

:zap: Sucht dann im Internet, wie Ihr playsound verwenden könnt!

<details>
  <summary>Ideen für Suchworte </summary>
  "python playsound example" oder "python playsound einfaches Beispiel"
</details>

<details>
   <summary>File not found :( </summary>
    Wenn Ihr eine Fehlermeldung bekommt, dass die Datei nicht gefunden wurde, dann liegt das wahrscheinlich daran, dass
    Python an der falschen Stelle sucht oder die falsche Datei sucht. Schaut genau, ob die Datei im selben Ordner liegt
    und ob Ihr den richtigen Namen angegeben habt.
</details>


Wofür könnte das nützlich sein? :zap: Baut einen Tee-Wecker, der fragt, wie lang der Tee ziehen soll, und dann nach der
entsprechenden Zeit das Soundfile abspielt.

<details>
   <summary>Eine bestimmte Zeit warten </summary>
   Das könnt Ihr mit der Funktion `sleep` aus dem Modul `time` machen. Wenn Ihr Euch da nicht mehr dran erinnert, schlagt es im Netz nach!
</details>