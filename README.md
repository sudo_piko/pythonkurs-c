# Pythonkurs Clownfisch – Generative Art

Hi! Wenn Ihr mit diesen Materialien lernt, ohne im Kurs dabei zu sein, dann sendet mir doch mal einen lieben Gruß, 
zB auf Mastodon: piko@chaos.social

Ich freu mich immer sehr, wenn ich mitbekomme, dass der Kurs mehr Leuten hilft!

![](https://wauland.de/images/logo-blue.svg)

### Die Letzte Stunde
Ist am 27.07.2023.

Am 03.08.2023 ist dann Party!


# Infos
Python für absolute Anfänger*innen

- Start: 01.12.2022
- Donnerstags, 19:30 - 21:00
- bis Ende Juli 2023
- Thema: schöne Dinge bauen; generative Art




Der Pythonkurs für Absolute Anfänger\*innen geht ab Dezember in die nächste Runde. Eingeladen sind alle FINTA 
Personen, also Frauen, agender, inter, nonbinary, trans und agender Personen, die gerne programmieren lernen möchten;
ganz besonders die, die sich für einen hoffnungslosen Fall halten. Wir fangen bei **null** Programmiererfahrung an. 
Der Kurs wird sehr übungslastig sein; der Fokus des Kureses liegt darauf, schöne Dinge automatisch erstellen zu lassen,
also Generative Kunst. Ich habe der Wau-Holland-Stiftung[1] zu danken, deren Förderung mir ermöglicht, den Kurs kostenlos anzubieten!

Wir treffen uns ab dem 01.12. bis in den Sommer wöchentlich **donnerstagabends 19:30** in einem BigBlueButton-Raum. Zusätzlich wird es Übungen geben und Ihr werdet Euch regelmäßig in Kleingruppen treffen.

Schreibt bei Interesse eine Mail an piko@riseup.net mit einer kurzen Vorstellung und Eurer Motivation, warum Ihr Programmieren lernen möchtet – von dort gibt es dann zeitnah eine Mail mit den Informationen für das erste Treffen.

[1] https://wauland.de/de/